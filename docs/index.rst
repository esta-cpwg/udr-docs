.. Uniform Device Representation documentation master file, created by
   sphinx-quickstart on Sat Jun  6 15:09:41 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

##################################################
ESTA BSR E1.73 Uniform Device Representation (UDR)
##################################################

.. warning::
    This documentation is for an outdated version of UDR. It does not reflect the current form of
    UDR as specified by the draft standard being worked on by the ESTA E1.73 task group. This site
    will be updated soon.

**E1.73 Next Generation Entertainment Control Model: Uniform Device Representation** is a method
for describing controllable devices in a machine-readable way. UDR is developed by the
Entertainment Services and Technology Association (ESTA) and has applications in entertainment
technology, architectural lighting and building automation, and the Internet of Things.

Data Model
==========

.. toctree::
   :maxdepth: 2
   :caption: Tutorial

   tutorial/introduction
   tutorial/identifiers
   tutorial/structured_data

.. toctree::
   :maxdepth: 2
   :caption: Data Model Objects

   model/data_model_objects/root
   model/data_model_objects/library
   model/data_model_objects/import
   model/data_model_objects/system
   model/data_model_objects/category
   model/data_model_objects/device_class
   model/data_model_objects/device_instance
   model/data_model_objects/device_library
   model/data_model_objects/scalar_items
   model/data_model_objects/structured_items
   model/data_model_objects/streaming_structured_items
   model/data_model_objects/bulk_items
   model/data_model_objects/localizations
   model/data_model_objects/localization
   model/data_model_objects/localized
   model/data_model_objects/case

.. toctree::
   :maxdepth: 2
   :caption: Atomic Data Objects

   model/atomic_data_objects/scalar_item_class
   model/atomic_data_objects/scalar_item_instance
   model/atomic_data_objects/structured_item_class
   model/atomic_data_objects/structured_item_instance
   model/atomic_data_objects/streaming_structured_item_class
   model/atomic_data_objects/streaming_structured_item_instance
   model/atomic_data_objects/streaming_item
   model/atomic_data_objects/bulk_item_class
   model/atomic_data_objects/bulk_item_instance

.. toctree::
   :maxdepth: 2
   :caption: Other Resources

   model/other_resources/json_key_reference

.. toctree::
   :maxdepth: 2
   :caption: Core Library

   libraries/core/full_definition_list
   libraries/core/categories
   libraries/core/structured_items/constraints
   libraries/core/structured_items/control_groups
   libraries/core/streaming_structured_items/dmx-profile

.. toctree::
   :maxdepth: 2
   :caption: Intensity Library

   libraries/intensity/full_definition_list
   libraries/intensity/categories
   libraries/intensity/scalar_items/intensity
   libraries/intensity/scalar_items/binary-intensity

.. toctree::
   :maxdepth: 2
   :caption: Position Library

   libraries/position/full_definition_list
   libraries/position/categories

.. toctree::
   :maxdepth: 2
   :caption: Color Library

   libraries/color/full_definition_list
   libraries/color/categories
   libraries/color/scalar_items/spectrum-data
   libraries/color/scalar_items/spectrum-wavelength-start
   libraries/color/scalar_items/spectrum-wavelength-end
   libraries/color/scalar_items/spectrum-wavelength-step

.. toctree::
   :maxdepth: 2
   :caption: Beam Library

   libraries/beam/full_definition_list
   libraries/beam/categories

.. toctree::
   :maxdepth: 2
   :caption: Effects Library

   libraries/effects/full_definition_list
   libraries/effects/categories

.. toctree::
   :maxdepth: 2
   :caption: Identification Library

   libraries/identification/full_definition_list
   libraries/identification/categories
   libraries/identification/scalar_items/manufacturer
   libraries/identification/scalar_items/model

.. toctree::
   :maxdepth: 2
   :caption: Examples
   
   examples/simple_dimmer
   examples/multi_cell
   examples/mac_encore
