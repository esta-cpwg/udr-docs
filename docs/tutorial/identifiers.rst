.. _tutorial-identifiers:

###########
Identifiers
###########

Atomic data object classes and instances often need to be uniquely and globally identified. Classes and instances
are identified by an **id**.

Identifiers must be unique within the library, device or system class containing the object being defined.

.. _tutorial-identifiers-referencing:

Referencing Objects
===================

When referencing defined atomic data objects, the identifier may either be relative or fully qualified. Relative
identifiers refer to objects defined in the same library, device or system class, while fully
qualified identifiers reference objects defined in other libraries or device classes.

All fully qualified identifiers begin with a :ref:`Qualified Identifier<tutorial-identifiers-qualified-identifier>`,
and are followed by the unique identifier separated by a ``/``.

Relative identifiers begin with a ``/`` and are always reference from the root of the containing library, device or
system.

If object classes are found nested inside :ref:`categories<data-model-objects-category>`, their URI shall include the
ids of the category hierarchy within the path, for instance: ``org.esta.lib.intensity.1/strobe/waveform``.

The following characters are reserved, and shall not be used in class or instance identifiers.

=========== =================== ==========================================================================
Reserved    ? # [ ] @ $ . :     These characters are reserved and may not be used
Path        /                   Delimiter for path components
Instance    !                   Delimiter for the instance number of a repeating atomic data object
Modifier    ;                   Delimiter for modifiers
=========== =================== ==========================================================================

.. _tutorial-identifiers-organization-id:

***********************
Organization Identifier
***********************

An organization identifier namespaces other classes under a given organization. By convention, an organization
identifier is an organization's registered domain name, reversed; for example, ACME corp headquartered at
acme.com would begin a qualified identifiers with ``com.acme``.

This follows the convention of `reverse domain name notation <https://en.wikipedia.org/wiki/Reverse_domain_name_notation>`_.

.. _tutorial-identifiers-qualified-identifier:

********************
Qualified Identifier
********************

:ref:`Libraries<data-model-objects-library>` and :ref:`Device Classes<data-model-objects-device-class>`
are identified by a qualified identifier. A qualified identifier is a string that begins with an
:ref:`tutorial-identifiers-organization-id`.

The organization identifier is followed by a type identifier, separated by a dot. Type identifiers can be any
of the following:

==================  ===============
Object              Type Identifier
==================  ===============
Library             lib
Device Class        dev
System              sys
==================  ===============

The type identifier is followed by an identifying string for the library or device, separated by a dot.

The last part of the qualified identifier is a version number. This must be an integer in string
representation, which must increment when any change is made to a published library or device.

Putting it all together, version 1 of ACME's color library could have the qualified identifier
``com.acme.lib.color.1``, or version 2 of ACME's *SuperLight* could have the qualified identifier
``com.acme.dev.superlight.2``.