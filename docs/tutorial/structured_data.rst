.. _tutorial-structured-data:

###############
Structured Data
###############

WORK IN PROGRESS

In UDR structured data is use to represent relationships between other atomic data objects (scalar items,
bulk items etc.).

Although the format of each item using structured data is defined within the context of that specific
item's class, there are some requirements for all structured data items (structured item and streaming structured
item).

************
Requirements
************

All structured data items shall use JSON.

When referencing other atomic data objects (scalar items, bulk items etc.), the following attributes shall
be used:

======================= ========================
scalaritem              Scalar Items
structureditem              Structured Item        
streamingstructureditem     Streaming Structured Item    
bulkitem                Bulk Item
======================= ========================

Example: ``<arbitrary><anotherarbitrary bulkitem="/blk/some-bulk-item-id"></anotherarbitrary></arbitrary>``
