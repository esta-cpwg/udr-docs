.. _atomic-data-objects-scalar-item-class:

#################
Scalar Item Class
#################

Data items represent a single piece of data related to a device.

A scalar item is a data item which represents a simple value type such as an integer or string. A
**scalar item class** provides information about how to read (and write, if supported) the data
associated with a scalar item. A scalar item class defines several attributes about a scalar item.

**********
Attributes
**********

Class attributes are part of the scalar item class; they apply to all *instances* of a scalar item of
this class.

.. _atomic-data-objects-scalar-item-class-id:

ID (Required)
=============

A scalar item class's **id** is a string identifying an individual scalar item class. This must be
unique within the :ref:`data-model-objects-library` or :ref:`data-model-objects-scalar-items` object
that contains the scalar item class.

.. _atomic-data-objects-scalar-item-class-name:

Name (Required)
===============

A string containing the human-readable name of the scalar item.

.. _atomic-data-objects-scalar-item-class-description:

Description
===========

A string that describes the purpose of a scalar item in human-readable language.

.. _atomic-data-objects-scalar-item-class-data-type:

Data Type (Required*)
====================

A scalar item class's **datatype** specifies how to interpret the data stored with the scalar item.

\*Data Type is not required when the scalar item class includes the **extends** attribute.
This attribute should be excluded, and will be ignored if **extends** is included.

The possible types are:

========= ========================================================
number    A numeric value, either integral or floating point.
string    A UTF-8 string.
binary    Opaque binary data.
boolean   A boolean value (true or false).
enum      A numeric integral value.
uuid      A universally unique identifier compliant with RFC 4122.
========= ========================================================

.. _atomic-data-objects-scalar-item-class-unit:

Unit (Required*)
================

A scalar item class's **unit** defines how the scalar item's value should be interpreted. Units are
only well-defined for scalar items with data type *number*.

\*Unit is only required when the data type attribute is included, and is defined as *number*. This
attribute should be excluded, and will be ignored if **extends** is included. 

======================== ======================================================
Unit Identifier          Description
======================== ======================================================
degree-celsius           Temperature in Degrees Celsius
degree-fahrenheit        Temperature in Degrees Fahrenheit
kelvin                   Temperature in Kelvin
volt-dc                  DC Voltage in Volts
volt-ac-peak             Peak AC Voltage in Volts
volt-ac-rms              RMS AC Voltage in Volts
amp-dc                   DC Current in Amperes
amp-ac-peak              Peak AC Current in Amperes
amp-ac-rms               RMS AC Current in Amperes
hertz                    Frequency in Hertz
ohm                      Resistance in Ohms
watt                     Power in Watts
kilogram                 Mass in Kilograms
meter                    Length or Position in Meters
square-meter             Area in Square Meters
cubic-meter              Volume in Cubic Meters
meter-per-second         Velocity in Meters per Second
meter-per-second-squared Acceleration in Meters per Second Squared
newton                   Force in Newtons
joule                    Energy in Joules
pascal                   Pressure in Pascals
second                   Time in Seconds
degree                   Angle in Degrees
steradian                Angle in Steradians
candela                  Luminous Intensity in Candela
lumen                    Luminous Flux in Lumens
lux                      Illuminance in Lux
ire                      Chrominance in IRE
byte                     Data Size in Bytes
percent                  Percentage of a Maximum Value (1.0 = Maximum Value)
======================== ======================================================

.. _atomic-data-objects-scalar-item-class-default:

Default
=======

This attribute defines a default value for the scalar item class. It applies in different ways when combined
with different data types. If this value is outside of the min/max range (either fixed or calculated) in a
scalar item of this class, the nearest in range value shall be used.

If a default is also provided in a :ref:`scalar item<atomic-data-objects-scalar-item-instance>` it overrides the
value defined here.

Default when applied to...

================== ===============================================
When applied to... Default indicates...
================== ===============================================
number             The number's default value.
string             N/A
binary             N/A
boolean            N/A
enum               The default enumerated index.
object             N/A
================== ===============================================

.. _atomic-data-objects-scalar-item-class-extends:

Extends
=======

Indicates the :ref:`id<atomic-data-objects-scalar-item-class-id>` of the scalar item class
this class extends. Scalar items may be extended for the purpose of renaming another
previously defined scalar item class.

.. _atomic-data-objects-scalar-item-class-markup:

******
Markup
******

* UDR Type: ``scalaritemclass``
* Members:

  =========== ========== =======================================================
  Key         Value Type Represents
  =========== ========== =======================================================
  id          string     :ref:`atomic-data-objects-scalar-item-class-id`
  name        string     :ref:`atomic-data-objects-scalar-item-class-name`
  description string     :ref:`atomic-data-objects-scalar-item-class-description`
  datatype    string     :ref:`atomic-data-objects-scalar-item-class-data-type`
  unit        string     :ref:`atomic-data-objects-scalar-item-class-unit`
  extends     string     :ref:`atomic-data-objects-scalar-item-class-extends`
  =========== ========== =======================================================

Example:

.. code-block:: json

  {
    "udrtype": "scalaritemclass",
    "id": "strobe-frequency",
    "name": "Strobe Frequency",
    "description": "The frequency of a strobe shutter in Hertz",
    "datatype": "number",
    "unit": "hertz"
  }
