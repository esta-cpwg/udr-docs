.. _atomic-data-objects-structured-item-class:

#####################
Structured Item Class
#####################

Data items represent a single piece of data related to a device.

A structured item is a data item which represents :ref:`structured data<tutorial-structured-data>`. A
**structured item class** provides information about how to read (and write, if supported) the data
associated with a structured item. A structured item class defines several attributes about a structured item.

**********
Attributes
**********

Class attributes are part of the structured item class; they apply to all *instances* of a structured item of
this class.

.. _atomic-data-objects-structured-item-class-id:

ID (Required)
=============

A structured item class's **id** is a string identifying an individual structured item class. This must be
unique within the :ref:`data-model-objects-library` or :ref:`data-model-objects-structured-items` object
that contains the structured item class.

.. _atomic-data-objects-structured-item-class-name:

Name (Required)
===============

A string containing the human-readable name of the structured item.

.. _atomic-data-objects-structured-item-class-description:

Description
===========

A string that describes the purpose of a structured item in human-readable language.

.. _atomic-data-objects-structured-item-class-markup:

******
Markup
******

* UDR Type: ``structureditemclass``
* Members:

  =========== ========== =======================================================
  Key         Value Type Represents
  =========== ========== =======================================================
  id          string     :ref:`atomic-data-objects-structured-item-class-id`
  name        string     :ref:`atomic-data-objects-structured-item-class-name`
  description string     :ref:`atomic-data-objects-structured-item-class-description`
  =========== ========== =======================================================

Example:

.. code-block:: json

  {
    "udrtype": "structureditemclass",
    "id": "constraints",
    "name": "Constraints",
    "description": "Contains markup defining availability and valid ranges of scalar items"
  }
