.. _atomic-data-objects-scalar-item-instance:

###########
Scalar Item
###########

A data item is a single controllable and/or observable piece of data.

A scalar item is a data item which represents a simple value type such as an integer or string. Scalar items
standardize how data is interpreted, how data can be accessed, and any constraints on the value of data.

A **scalar item** is a single addressable instance of a :doc:`scalar_item_class`.

In a static UDR file, scalar items must be children of the :ref:`scalar items<data-model-objects-scalar-items>`
object.

**********
Attributes
**********

Instance attributes are per-scalar-item-instance; they can vary between instances of the scalar item.

.. _atomic-data-objects-scalar-item-instance-class:

Class (Required)
================

Indicates the :ref:`id<atomic-data-objects-scalar-item-class-id>` of the defined scalar item class of which this
is an instance. If the scalar item class is nested within one or more categories, the full path must include the
category id hierarchy.

.. _atomic-data-objects-scalar-item-instance-id:

ID (Required)
=============

The **id** is a unique string identifying this scalar item. This must be unique within the
:ref:`scalar items<data-model-objects-scalar-items>` object within the device class containing the scalar item
(or list of scalar items retrieved from a device), unless it is paired with a revision of the same class using
the same id (see :ref:`versioning<data-model-objects-device-class-versioning>`).

If multiple scalar items are instantiated using the *count* attribute, each instance will be identified
starting with the unique id followed by the ``!`` delimiter and instance number (starting at 1), and incrementing
by 1 for each instance. For example "id!1" would be followed by "id!2".

When ordering of multiple scalar item instances of the same class is required, they shall be instantiated either
using the *count* mechanism described above, or may be manually instantiated with the same unique id followed
by the ``!`` delimiter and an instance number.

The id must remain consistent through revisions of a :ref:`data-model-objects-device-class`, as this uniquely
identifies the scalar item.

An example of a complete scalar item instance id for the second intensity scalar item in a device where there are 3
revisions of the intensity scalar item presented might take the form: ``intensity!2.3``.

.. _atomic-data-objects-scalar-item-instance-friendly-name:

Friendly Name
=============

The **friendly name** attribute provides a string that could be used to identify a particular instance
of a scalar item in a software user interface, in order to provide more context to a user. It is not
intended to uniquely identify the scalar item in a database.

.. _atomic-data-objects-scalar-item-instance-count:

Count
=====

The **count** attribute provides a mechanism for instantiating multiple scalar item instances of the same class.

If multiple scalar items are instantiated using the *count* attribute, each instance will be identified
starting with the unique id followed by the ``!`` delimiter and instance number (starting at 1), and incrementing
by 1 for each instance. For example "id!1" would be followed by "id!2".

.. _atomic-data-objects-scalar-item-instance-access-and-lifetime:

Access and Lifetime (Required)
==============================

Access and Lifetime define how the data associated with a scalar item is stored, how it can be changed, and when it
is invalidated.

**Access** defines whether the value of a scalar item can be changed by a user.

* **Read-Only** scalar items cannot be changed.
* **Read-Write** scalar items can be changed.

**Lifetime** defines how the value of a scalar item is stored.

* **Static** scalar items apply to all instances of a device. This data may or may not be
  retrievable from an individual device.
* **Persistent** scalar items are individual to a specific device instance, and their data is
  retained between device power cycles.
* **Runtime** scalar items are individual to a specific device instance, and their data is not
  retained between device power cycles.

.. list-table:: Access and Lifetime Grid
   :header-rows: 1
   :widths: auto

   * - 
     - Read-Only
     - Read-Write
   * - Static
     - Data that is the same for all instances (units) of a device class.

       **Example:** Device Manufacturer.
     - N/A
   * - Persistent
     - Data that is per-device-instance and stored persistently but cannot be changed from outside
       the device.

       **Example:** Serial Number.
     - Data that is per-device-instance and stored persistently and can be changed from outside
       the device.

       **Example:** User-Assigned Device Name.
   * - Runtime
     - Data that is measured or calculated at runtime and is not retained between power cycles.
       This is typically status and health data.

       **Example:** Temperature Sensors.
     - Data that can be changed from outside the device that is not retained between power cycles.
       This is typically data that is used to trigger the device to do an action.

       **Example:** :ref:`Intensity<library-intensity-scalar-item-intensity>`.

.. _atomic-data-objects-scalar-item-instance-minimum:

Minimum
=======

This attribute defines a minimum value for the scalar item. It applies in different ways when combined
with different data types. If this attribute and **minscalaritem** is absent, the minimum is assumed to
be 0:

================== ===============================================
When applied to... Minimum indicates...
================== ===============================================
number             The number's minimum value.
string             N/A
binary             N/A
boolean            N/A
enum               N/A
================== ===============================================

.. _atomic-data-objects-scalar-item-instance-maximum:

Maximum
=======

This attribute defines a maximum value for the scalar item. It applies in different ways when combined
with different data types. If this attribute and **maxscalaritem** is absent, the maximum is assumed to
be 0:

Maximum when applied to...

================== ===============================================
When applied to... Maximum indicates...
================== ===============================================
number             The number's maximum value.
string             N/A
binary             N/A
boolean            N/A
enum               The maximum enumerated index.
================== ===============================================

.. _atomic-data-objects-scalar-item-instance-default:

Default
=======

This attribute defines a default value for the scalar item. It applies in different ways when combined
with different data types. If this value falls outside of the defined or calculated (from one
or more other scalar items) min/max, then the nearest in range value shall be used.

If this attribute is provided it overrides any default defined in the
:ref:`scalar item class<atomic-data-objects-scalar-item-class>` of which this is an instance.

Default when applied to...

================== ===============================================
When applied to... Default indicates...
================== ===============================================
number             The number's default value.
string             N/A
binary             N/A
boolean            N/A
enum               The default enumerated index.
================== ===============================================

.. _atomic-data-objects-scalar-item-instance-minimum-scalar-item:

Minimum scalar item
=================

Indicates the modifier type, and **id** of another scalar item instance which this scalar item should
observe for a new minimum value. This should take the form ``modifiertype;id``. The modifier type
must be one of those defined below, and the id must be a valid :ref:`identifier<tutorial-identifiers>`.

When this attribute is defined and the modifier type is absolute, any discrete value provided in
**minimum** will be ignored.

For scalar items with a :ref:`data type<atomic-data-objects-scalar-item-class-data-type>`
of *number* this can be any of the following (all other types shall always use absolute):

============= ===============================================================
addend        The value provided should be added to that previously defined
multiplier    The value provided is a multiplier to that previously defined
absolute      The value provided should replace that previously defined
============= ===============================================================

.. _atomic-data-objects-scalar-item-instance-maximum-date-item:

Maximum scalar item
=================

Indicates the modifier type, and **id** of another scalar item instance which this scalar item should
observe for a new maximum value. This should take the form ``modifiertype;id``. The modifier type
must be one of those defined below, and the id must be a valid :ref:`identifier<tutorial-identifiers>`.

When this attribute is defined and the modifier type is absolute, any discrete value provided in
**maximum** will be ignored.

For scalar items with a :ref:`data type<atomic-data-objects-scalar-item-class-data-type>`
of *number* this can be any of the following (all other types shall always use absolute):

============= ===============================================================
addend        The value provided should be added to that previously defined
multiplier    The value provided is a multiplier to that previously defined
absolute      The value provided should replace that previously defined
============= ===============================================================

********
Children
********

Scalar items may not have any children.

.. _atomic-data-objects-scalar-item-instance-markup:       

******
Markup
******

* UDR Type: ``scalaritem``
* Members:

  ============= ========== =======================================================
  Key           Value Type Represents
  ============= ========== =======================================================
  class         string     :ref:`atomic-data-objects-scalar-item-instance-class`
  id            string     :ref:`atomic-data-objects-scalar-item-instance-id`
  friendlyname  string     :ref:`atomic-data-objects-scalar-item-instance-friendly-name`
  count         string     :ref:`atomic-data-objects-scalar-item-instance-count`
  access        string     :ref:`Access<atomic-data-objects-scalar-item-instance-access-and-lifetime>`
  lifetime      string     :ref:`Lifetime<atomic-data-objects-scalar-item-instance-access-and-lifetime>`
  minimum       number     :ref:`atomic-data-objects-scalar-item-instance-minimum`
  maximum       number     :ref:`atomic-data-objects-scalar-item-instance-maximum`
  default       number     :ref:`atomic-data-objects-scalar-item-instance-default`
  minscalaritem string     :ref:`atomic-data-objects-scalar-item-instance-minimum-scalar-item`
  maxscalaritem string     :ref:`atomic-data-objects-scalar-item-instance-maximum-scalar-item`
  ============= ========== =======================================================

Example:

.. code-block:: json

  {
    "udrtype": "scalaritem",
    "class": "org.esta.lib.identification.1/firmware-version",
    "id": "my-firmware-version",
    "friendlyname": "Firmware Version",
    "access": "readonly",
    "lifetime": "persistent"
  }
