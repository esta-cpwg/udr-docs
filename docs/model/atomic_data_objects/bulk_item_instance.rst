.. _atomic-data-objects-bulk-item-instance:

#########
Bulk Item
#########

A bulk item represents a file or other large binary object. The file may be a 3D model, an image,
movie, firmware or other file associated with a device. Bulk items standardize how file data is
interpreted, how data can be accessed, and any constraints on the file data.

A **bulk item** is a single addressable instance of a :doc:`bulk_item_class`.

In a static UDR file, bulk items must be children of the :ref:`bulkitems<data-model-objects-bulkitems>`
object within a device class:

**********
Attributes
**********

Instance attributes are per-bulk-item-instance; they can vary between instances of the bulk item.

.. _atomic-data-objects-bulk-item-instance-class:

Class (Required)
================

Indicates the :ref:`id<atomic-data-objects-bulk-item-class-id>` of the defined bulk item class of which
this is an instance. If the bulk item class is nested within one or more categories, the full path must include
the category id hierarchy.

.. _atomic-data-objects-bulk-item-instance-id:

ID (Required)
=============

The **id** is a unique string identifying this bulk item. This must be unique within the parent container
(bulkitems) within the device class containing the bulk item (or list of bulk items retrieved from a device),
unless it is paired with a revision of the same class using the same id
(see :ref:`versioning<data-model-objects-device-class-versioning>`).

If multiple bulk items are instantiated using the *count* attribute, each instance will be identified
starting with the unique id followed by the ``!`` delimiter and instance number (starting at 1), and incrementing
by 1 for each instance. For example "id!1" would be followed by "id!2".

When ordering of multiple bulk item instances of the same class is required, they shall be instantiated either
using the *count* mechanism described above, or may be manually instantiated with the same unique id followed
by the ``!`` delimiter and an instance number.

The id must remain consistent through revisions of a :ref:`data-model-objects-device-class`, as this uniquely
identifies the bulk item.

An example of a complete bulk item instance id for the second gobo-image bulk item in a device where there are 3
revisions of the gobo-image bulk item presented might take the form: ``gobo-image!2.3``.

.. _atomic-data-objects-bulk-item-instance-friendly-name:

Friendly Name
=============

The **friendly name** attribute provides a string that could be used to identify a particular instance
of a bulk item in a software user interface, in order to provide more context to a user. It is not
intended to uniquely identify the bulk item in a database.

.. _atomic-data-objects-bulk-item-instance-count:

Count
=====

The **count** attribute provides a mechanism for instantiating multiple bulk item instances of the same class.

If multiple bulk items are instantiated using the *count* attribute, each instance will be identified
starting with the unique id followed by the ``!`` delimiter and instance number (starting at 1), and incrementing
by 1 for each instance. For example "id!1" would be followed by "id!2".

.. _atomic-data-objects-bulk-item-instance-access-and-lifetime:

Access and Lifetime (Required)
==============================

Access and Lifetime define how the data associated with a bulk item is stored, how it can be changed, and when it
is invalidated.

**Access** defines whether the value of a bulk item can be changed by a user.

* **Read-Only** bulk items cannot be changed.
* **Read-Write** bulk items can be changed.

**Lifetime** defines how the file found at the URL value of a bulk item is stored.

* **Static** bulk items apply to all instances of a device. This data may or may not be
  retrievable from an individual device.
* **Persistent** bulk items are individual to a specific device instance, and their data is
  retained between device power cycles.
* **Runtime** bulk items are individual to a specific device instance, and their data is not
  retained between device power cycles.

.. list-table:: Access and Lifetime Grid
   :header-rows: 1
   :widths: auto

   * - 
     - Read-Only
     - Read-Write
   * - Static
     - Data that is the same for all instances (units) of a device class.

       **Example:** 3D Model.
     - N/A
   * - Persistent
     - Data that is per-device-instance and stored persistently but cannot be changed from outside
       the device.

       **Example:** Gobo Images.
     - Data that is per-device-instance and stored persistently and can be changed from outside
       the device.

       **Example:** NEED EXAMPLE
   * - Runtime
     - Data that is measured or calculated at runtime and is not retained between power cycles.
       This is typically status and health data.

       **Example:** NEED EXAMPLE
     - Data that can be changed from outside the device that is not retained between power cycles.

       **Example:** NEED EXAMPLE

********
Children
********

Bulk items may not have any children.

.. _atomic-data-objects-bulk-item-instance-markup:       

******
Markup
******

* UDR Type: ``bulkitem``
* Members:

  ============= ========== =======================================================
  Key           Value Type Represents
  ============= ========== =======================================================
  class         string     :ref:`atomic-data-objects-bulk-item-instance-class`
  id            string     :ref:`atomic-data-objects-bulk-item-instance-id`
  friendlyname  string     :ref:`atomic-data-objects-bulk-item-instance-friendly-name`
  count         string     :ref:`atomic-data-objects-bulk-item-instance-count`
  access        string     :ref:`Access<atomic-data-objects-bulk-item-instance-access-and-lifetime>`
  lifetime      string     :ref:`Lifetime<atomic-data-objects-bulk-item-instance-access-and-lifetime>`
  ============= ========== =======================================================

Example:

.. code-block:: json

  {
    "udrtype": "bulkitem",
    "class": "org.esta.lib.gobo.1/gobo-image",
    "id": "gobo-image!1",
    "access": "readonly",
    "lifetime": "persistent"
  }
