.. _atomic-data-objects-streaming-structured-item-instance:

#########################
Streaming Structured Item
#########################

A data item is a single controllable and/or observable piece of data.

A streaming structured item is a data item which represents :ref:`structured data<tutorial-structured-data>`. This structured
data defines how other scalar and structured items can be packaged together for a streaming transport method such as sACN E1.31
/ DMX-512 E1.11. Streaming structured items standardize how data is interpreted, how data can be accessed, and any constraints
on the value of data.

A **streaming structured item** is a single addressable instance of a :doc:`streaming_structured_item_class`.

In a static UDR file, streaming structured items must be children of the
:ref:`streaming structured items<data-model-objects-streaming-structured-items>` object.

**********
Attributes
**********

Instance attributes are per-streaming-structured-item-instance; they can vary between instances of the streaming structured item.

.. _atomic-data-objects-streaming-structured-item-instance-class:

Class (Required)
================

Indicates the :ref:`id<atomic-data-objects-streaming-structured-item-class-id>` of the defined streaming structured item class of
which this is an instance. If the streaming structured item class is nested within one or more categories, the full path must
include the category id hierarchy.

.. _atomic-data-objects-streaming-structured-item-instance-id:

ID (Required)
=============

The **id** is a unique string identifying this streaming structured item. This must be unique within the
:ref:`streaming structured items<data-model-objects-streaming-structured-items>` object within the device class containing the streaming
structured item (or list of streaming structured items retrieved from a device), unless it is paired with a revision of the same class
using the same id (see :ref:`versioning<data-model-objects-device-class-versioning>`).

If multiple streaming structured items are instantiated using the *count* attribute, each instance will be identified
starting with the unique id followed by the ``!`` delimiter and instance number (starting at 1), and incrementing
by 1 for each instance. For example "id!1" would be followed by "id!2".

When ordering of multiple streaming structured item instances of the same class is required, they shall be instantiated either
using the *count* mechanism described above, or may be manually instantiated with the same unique id followed
by the ``!`` delimiter and an instance number.

The id must remain consistent through revisions of a :ref:`data-model-objects-device-class`, as this uniquely
identifies the streaming structured item.

An example of a complete streaming structured item instance id for the second constraints streaming structured item in a device where
there are 3 revisions of the dnx profile streaming structured item presented might take the form: ``standard-dmx-profile!2.3``.

.. _atomic-data-objects-streaming-structured-item-instance-friendly-name:

Friendly Name
=============

The **friendly name** attribute provides a string that could be used to identify a particular instance
of a streaming structured item in a software user interface, in order to provide more context to a user. It is not
intended to uniquely identify the streaming structured item in a database.

.. _atomic-data-objects-streaming-structured-item-instance-count:

Count
=====

The **count** attribute provides a mechanism for instantiating multiple streaming structured item instances of the same class.

If multiple streaming structured items are instantiated using the *count* attribute, each instance will be identified
starting with the unique id followed by the ``!`` delimiter and instance number (starting at 1), and incrementing
by 1 for each instance. For example "id!1" would be followed by "id!2".

.. _atomic-data-objects-streaming-structured-item-instance-access-and-lifetime:

Access and Lifetime (Required)
==============================

Access and Lifetime define how the data associated with a streaming structured item is stored, how it can be changed, and when it
is invalidated.

**Access** defines whether the value of a streaming structured item can be changed by a user.

* **Read-Only** streaming structured items cannot be changed.
* **Read-Write** streaming structured items can be changed.

**Lifetime** defines how the value of a streaming structured item is stored.

* **Static** streaming structured items apply to all instances of a device. This data may or may not be
  retrievable from an individual device.
* **Persistent** streaming structured items are individual to a specific device instance, and their data is
  retained between device power cycles.
* **Runtime** streaming structured items are individual to a specific device instance, and their data is not
  retained between device power cycles.

.. list-table:: Access and Lifetime Grid
   :header-rows: 1
   :widths: auto

   * - 
     - Read-Only
     - Read-Write
   * - Static
     - Data that is the same for all instances (units) of a device class.

       **Example:** DMX Profile.
     - N/A
   * - Persistent
     - Data that is per-device-instance and stored persistently but cannot be changed from outside
       the device.

       **Example:** Example Required.
     - Data that is per-device-instance and stored persistently and can be changed from outside
       the device.

       **Example:** Example Required.
   * - Runtime
     - Data that is measured or calculated at runtime and is not retained between power cycles.
       This is typically status and health data.

       **Example:** Example Required.
     - Data that can be changed from outside the device that is not retained between power cycles.
       This is typically data that is used to trigger the device to do an action.

       **Example:** Example Required.

********
Children
********

Streaming structured items may not have any children.

.. _atomic-data-objects-streaming-structured-item-instance-markup:       

******
Markup
******

* UDR Type: ``streamingstructureditem``
* Members:

  ============= ========== =======================================================
  Key           Value Type Represents
  ============= ========== =======================================================
  class         string     :ref:`atomic-data-objects-streaming-structured-item-instance-class`
  id            string     :ref:`atomic-data-objects-streaming-structured-item-instance-id`
  friendlyname  string     :ref:`atomic-data-objects-streaming-structured-item-instance-friendly-name`
  count         string     :ref:`atomic-data-objects-streaming-structured-item-instance-count`
  access        string     :ref:`Access<atomic-data-objects-streaming-structured-item-instance-access-and-lifetime>`
  lifetime      string     :ref:`Lifetime<atomic-data-objects-streaming-structured-item-instance-access-and-lifetime>`
  ============= ========== =======================================================

Example:

.. code-block:: json

  {
    "udrtype": "streamingstructureditem",
    "class": "org.esta.lib.core.1/dmx-profile",
    "id": "standard-mode",
    "friendlyname": "Standard Mode DMX Profile",
    "access": "readonly",
    "lifetime": "persistent"
  }
