.. _atomic-data-objects-bulk-item-class:

###############
Bulk Item Class
###############

A bulk item represents a file or other large binary object. The file may be a 3D model, an image,
movie, firmware or other file associated with a device. A **bulk item class** provides information
about how to read (and write, if supported) the file data associated with a bulk item. A bulk item
class defines several attributes about a bulk item.

**********
Attributes
**********

Class attributes are part of the bulk item class; they apply to all *instances* of a bulk item of
this class.

.. _atomic-data-objects-bulk-item-class-id:

ID (Required)
=============

A bulk item class's **id** is a string identifying an individual bulk item class. This must be
unique within the :ref:`data-model-objects-library` or parent object that contains the bulk item
class.

.. _atomic-data-objects-bulk-item-class-name:

Name (Required)
===============

A string containing the human-readable name of the bulk item.

.. _atomic-data-objects-bulk-item-class-description:

Description
===========

A string that describes the purpose of a bulk item in human-readable language.

.. _atomic-data-objects-bulk-item-class-file-type:

File Type (Required)
====================

A bulk item class's **filetype** specifies how to interpret the data stored with the bulk item.

The possible types are:

========= ==============================================
movie     Represents video containing audio
video     Represents video that doesn't contain audio
image     Represents image data
audio     Represents audio that doesn't contain video
3dmodel   A 3D representation
symbol    A 2D symbol representation
other     All other file types
========= ==============================================

.. _atomic-data-objects-bulk-item-class-file-formats:

File Formats (Required)
=======================

A comma separated list of possible file extensions. File extensions are not case-sensitive.

Examples may include: jpg, obj, mov.

.. _atomic-data-objects-bulk-item-class-markup:

******
Markup
******

* UDR Type: ``bulkitemclass``
* Members:

  =========== ========== =======================================================
  Key         Value Type Represents
  =========== ========== =======================================================
  id          string     :ref:`atomic-data-objects-bulk-item-class-id`
  name        string     :ref:`atomic-data-objects-bulk-item-class-name`
  description string     :ref:`atomic-data-objects-bulk-item-class-description`
  filetype    string     :ref:`atomic-data-objects-bulk-item-class-file-type`
  fileformats string     :ref:`atomic-data-objects-bulk-item-class-file-formats`
  =========== ========== =======================================================

Example:

.. code-block:: json

  {
    "udrtype": "bulkitemclass",
    "id": "gobo-image",
    "name": "Gobo Image",
    "description": "The image of a gobo",
    "filetype": "image",
    "fileformats": "jpg,png"
  }
