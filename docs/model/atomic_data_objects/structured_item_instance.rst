.. _atomic-data-objects-structured-item-instance:

###############
Structured Item
###############

A data item is a single controllable and/or observable piece of data.

A structured item is a data item which represents :ref:`structured data<tutorial-structured-data>`. Structured items
standardize how data is interpreted, how data can be accessed, and any constraints on the value of data.

A **structured item** is a single addressable instance of a :doc:`structured_item_class`.

In a static UDR file, structured items must be children of the :ref:`structured items<data-model-objects-structured-items>`
object.

**********
Attributes
**********

Instance attributes are per-structured-item-instance; they can vary between instances of the structured item.

.. _atomic-data-objects-structured-item-instance-class:

Class (Required)
================

Indicates the :ref:`id<atomic-data-objects-structured-item-class-id>` of the defined structured item class of which this
is an instance. If the structured item class is nested within one or more categories, the full path must include the
category id hierarchy.

.. _atomic-data-objects-structured-item-instance-id:

ID (Required)
=============

The **id** is a unique string identifying this structured item. This must be unique within the
:ref:`structured items<data-model-objects-structured-items>` object within the device class containing the structured item
(or list of structured items retrieved from a device), unless it is paired with a revision of the same class using
the same id (see :ref:`versioning<data-model-objects-device-class-versioning>`).

If multiple structured items are instantiated using the *count* attribute, each instance will be identified
starting with the unique id followed by the ``!`` delimiter and instance number (starting at 1), and incrementing
by 1 for each instance. For example "id!1" would be followed by "id!2".

When ordering of multiple structured item instances of the same class is required, they shall be instantiated either
using the *count* mechanism described above, or may be manually instantiated with the same unique id followed
by the ``!`` delimiter and an instance number.

The id must remain consistent through revisions of a :ref:`data-model-objects-device-class`, as this uniquely
identifies the structured item.

An example of a complete structured item instance id for the second constraints structured item in a device where there are 3
revisions of the intensity structured item presented might take the form: ``constraints!2.3``.

.. _atomic-data-objects-structured-item-instance-friendly-name:

Friendly Name
=============

The **friendly name** attribute provides a string that could be used to identify a particular instance
of a structured item in a software user interface, in order to provide more context to a user. It is not
intended to uniquely identify the structured item in a database.

.. _atomic-data-objects-structured-item-instance-count:

Count
=====

The **count** attribute provides a mechanism for instantiating multiple structured item instances of the same class.

If multiple structured items are instantiated using the *count* attribute, each instance will be identified
starting with the unique id followed by the ``!`` delimiter and instance number (starting at 1), and incrementing
by 1 for each instance. For example "id!1" would be followed by "id!2".

.. _atomic-data-objects-structured-item-instance-access-and-lifetime:

Access and Lifetime (Required)
==============================

Access and Lifetime define how the data associated with a structured item is stored, how it can be changed, and when it
is invalidated.

**Access** defines whether the value of a structured item can be changed by a user.

* **Read-Only** structured items cannot be changed.
* **Read-Write** structured items can be changed.

**Lifetime** defines how the value of a structured item is stored.

* **Static** structured items apply to all instances of a device. This data may or may not be
  retrievable from an individual device.
* **Persistent** structured items are individual to a specific device instance, and their data is
  retained between device power cycles.
* **Runtime** structured items are individual to a specific device instance, and their data is not
  retained between device power cycles.

.. list-table:: Access and Lifetime Grid
   :header-rows: 1
   :widths: auto

   * - 
     - Read-Only
     - Read-Write
   * - Static
     - Data that is the same for all instances (units) of a device class.

       **Example:** Constraints.
     - N/A
   * - Persistent
     - Data that is per-device-instance and stored persistently but cannot be changed from outside
       the device.

       **Example:** Example Required.
     - Data that is per-device-instance and stored persistently and can be changed from outside
       the device.

       **Example:** Example Required.
   * - Runtime
     - Data that is measured or calculated at runtime and is not retained between power cycles.
       This is typically status and health data.

       **Example:** Example Required.
     - Data that can be changed from outside the device that is not retained between power cycles.
       This is typically data that is used to trigger the device to do an action.

       **Example:** Example Required.

********
Children
********

Structured items may not have any children.

.. _atomic-data-objects-structured-item-instance-markup:       

******
Markup
******

* UDR Type: ``structureditem``
* Members:

  ============= ========== =======================================================
  Key           Value Type Represents
  ============= ========== =======================================================
  class         string     :ref:`atomic-data-objects-structured-item-instance-class`
  id            string     :ref:`atomic-data-objects-structured-item-instance-id`
  friendlyname  string     :ref:`atomic-data-objects-structured-item-instance-friendly-name`
  count         string     :ref:`atomic-data-objects-structured-item-instance-count`
  access        string     :ref:`Access<atomic-data-objects-structured-item-instance-access-and-lifetime>`
  lifetime      string     :ref:`Lifetime<atomic-data-objects-structured-item-instance-access-and-lifetime>`
  ============= ========== =======================================================

Example:

.. code-block:: json

  {
    "udrtype": "structureditem",
    "class": "org.esta.lib.core.1/constraints",
    "id": "standard-constraints",
    "friendlyname": "Constraints",
    "access": "readonly",
    "lifetime": "persistent"
  }
