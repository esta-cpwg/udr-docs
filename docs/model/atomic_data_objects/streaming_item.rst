.. _atomic-data-objects-streaming-item-instance:

##############
Streaming Item
##############

A streaming item represents a statically-defined or pre-negotiated packing of scalar item values in
such a way that they can be efficiently streamed.

The contents and ordering of the values in a streaming item are defined by a
:ref:`streaming structured item<atomic-data-objects-streaming-structured-item-instance>`.

Streaming items are transported between devices using a streaming transport mechanism, and are never
present in a static UDR file.

**********
Attributes
**********

.. _atomic-data-objects-streaming-item-instance-streaming-structured-item:

Streaming Structured Item (Required)
====================================

Indicates the :ref:`id<atomic-data-objects-streaming-structured-item-instance-id>` of the defined
streaming structured item which describes the contents and ordering of the values in the data of this
item.

Data (Required)
===============

An array of octets containing the packed values for this item.

********
Children
********

Streaming items may not have any children.

.. _atomic-data-objects-streaming-item-instance-markup:       

******
Markup
******

* UDR Type: ``streamingitem``
* Members:

  ======================== ========== =======================================================
  Key                      Value Type Represents
  ======================== ========== =======================================================
  streamingstructureditem  string     :ref:`atomic-data-objects-streaming-item-instance-streaming-structured-item`
  data                     binary     :ref:`atomic-data-objects-bulk-item-instance-id`
  ======================== ========== =======================================================

Example:

.. code-block:: json

  {
    "udrtype": "streamingitem",
    "streamingstructureditem": "standard-mode",
    "data": "0000FFFF00000000"
  }

