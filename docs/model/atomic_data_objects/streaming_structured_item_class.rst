.. _atomic-data-objects-streaming-structured-item-class:

###############################
Streaming Structured Item Class
###############################

Data items represent a single piece of data related to a device.

A streaming structured item is a data item which represents :ref:`structured data<tutorial-structured-data>`.
This structured data defines how other scalar and structured items can be packaged together for a streaming
transport method such as sACN E1.31 / DMX-512 E1.11. A **streaming structured item class** provides information about
how to read (and write, if supported) the data associated with a streaming structured item. A streaming structured
item class defines several attributes about a streaming structured item.

**********
Attributes
**********

Class attributes are part of the streaming structured item class; they apply to all *instances* of a streaming structured
item of this class.

.. _atomic-data-objects-streaming-structured-item-class-id:

ID (Required)
=============

A streaming structured item class's **id** is a string identifying an individual streaming structured item class. This
must be unique within the :ref:`data-model-objects-library` or :ref:`data-model-objects-streaming-structured-items`
object that contains the streaming structured item class.

.. _atomic-data-objects-streaming-structured-item-class-name:

Name (Required)
===============

A string containing the human-readable name of the streaming structured item.

.. _atomic-data-objects-streaming-structured-item-class-description:

Description
===========

A string that describes the purpose of a streaming structured item in human-readable language.

.. _atomic-data-objects-streaming-structured-item-class-markup:

******
Markup
******

* UDR Type: ``streamingstructureditemclass``
* Members:

  =========== ========== =======================================================
  Key         Value Type Represents
  =========== ========== =======================================================
  id          string     :ref:`atomic-data-objects-streaming-structured-item-class-id`
  name        string     :ref:`atomic-data-objects-streaming-structured-item-class-name`
  description string     :ref:`atomic-data-objects-streaming-structured-item-class-description`
  =========== ========== =======================================================

Example:

.. code-block:: json

  {
    "udrtype": "streamingstructureditemclass",
    "id": "dmx-profile",
    "name": "DMX Profile",
    "description": "Describes a supported operating mode of a device and the dmx protocol translation in that mode."
  }
