##################
JSON Key Reference
##################

******************
Data Model Objects
******************

=========================== =============================================================================================
bulkitems                   :ref:`Bulk Items<data-model-objects-bulk-items-markup>`
case                        :ref:`Case<data-model-objects-case-markup>`
category                    :ref:`Category<data-model-objects-category-markup>`
device                      :ref:`Device<data-model-objects-device-instance-markup>`
deviceclass                 :ref:`Device Class<data-model-objects-device-class-markup>`
devicelibrary               :ref:`Device Library<data-model-objects-device-library-markup>`
import                      :ref:`Import<data-model-objects-import>`
library                     :ref:`Library<standard-objects-library-markup>`
localizations               :ref:`Localizations<data-model-objects-localizations-markup>`
localization                :ref:`Localization<data-model-objects-localization-markup>`
localized                   :ref:`Localized<data-model-objects-localized-markup>`
scalaritems                 :ref:`Scalar Items<data-model-objects-scalar-items-markup>`
structureditems             :ref:`Structured Items<data-model-objects-structured-items-markup>`
streamingstructureditems    :ref:`Streaming Structured Items<data-model-objects-streaming-structured-items-markup>`
system                      :ref:`System<data-model-objects-system-markup>`
udr                         :ref:`UDR Root<data-model-objects-udr-root-markup>`
=========================== =============================================================================================

*******************
Atomic Data Objects
*******************

=============================== =============================================================================================
bulkitem                        :ref:`Bulk Item<atomic-data-objects-bulk-item-instance-markup>`
bulkitemclass                   :ref:`Bulk Item Class<atomic-data-objects-bulk-item-class-markup>`
scalaritem                      :ref:`Scalar Item<atomic-data-objects-scalar-item-instance-markup>`
scalaritemclass                 :ref:`Scalar Item Class<atomic-data-objects-scalar-item-class-markup>`
streamingitem                   :ref:`Streaming Item<atomic-data-objects-streaming-item-instance-markup>`
streamingstructureditem         :ref:`Streaming Structured Item<atomic-data-objects-streaming-structured-item-instance-markup>`
streamingstructureditemclass    :ref:`Streaming Structured Item Class<atomic-data-objects-streaming-structured-item-class-markup>`
structureditem                  :ref:`Structured Item<atomic-data-objects-structured-item-instance-markup>`
structureditemclass             :ref:`Structured Item Class<atomic-data-objects-structured-item-class-markup>`
=============================== =============================================================================================