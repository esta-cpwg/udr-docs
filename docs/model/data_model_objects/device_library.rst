.. _data-model-objects-device-library:

##############
Device Library
##############

:ref:`Device Classes<data-model-objects-device-class>` can contain a maximum of one *devicelibrary* object which
lists all of the device specific manufacturer atomic data object classes (scalar item class, structured item class
etc.). These objects are made available within the context of the device.

When addressing children of the device library object, other objects should use ``/dl/`` followed by the class of
the atomic data object.

********
Children
********

Device library can have the following children:

* :ref:`atomic-data-objects-scalar-item-class`
* :ref:`atomic-data-objects-structured-item-class`
* :ref:`atomic-data-objects-streaming-structured-item-class`
* :ref:`atomic-data-objects-bulk-item-class`

.. _data-model-objects-device-library-markup:

Markup
======

* UDR Type: ``devicelibrary``

Example:

.. code-block:: json

  {
    "udrtype": "devicelibrary",
    "children": [
      {
        "udrtype": "scalaritemclass",
        "id": "my-scalaritem",
        "name": "My Scalar Item",
        "description": "Something not yet supported",
        "datatype": "number",
        "unit": "percent"
      }
    ]
  }
