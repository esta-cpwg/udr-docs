.. _data-model-objects-structured-items:

################
Structured Items
################

:ref:`Device Classes<data-model-objects-device-class>` can contain a maximum of one *structureditems*
object which list all of the controllable and/or observable structured items of the device. When addressing
children of the structured items object, other objects should use ``/str/`` followed by the id of the structured item.

********
Ordering
********

The order in which structured items appear has no meaning; structured items of the same class that appear in a
certain order in the structured items list should have no impact on the order any corresponding user interface
elements might be presented.

When ordering of structured items of the same class is required, they shall be instantiated with the same unique
identifier followed by the ``!`` delimiter and an instance number. The instance number shall be used to define
the order of like objects.

See :ref:`structured item<atomic-data-objects-structured-item-instance-id>` id description for further information.

********
Children
********

Structured items can have the following children:

* :ref:`atomic-data-objects-structured-item-instance`

.. _data-model-objects-structured-items-markup:

Markup
======

* UDR Type: ``structureditems``

Example:

.. code-block:: json

  {
    "udrtype": "structureditems",
    "children": [
      {
        "udrtype": "structureditem",
        "class": "org.esta.lib.core.1/constraints",
        "id": "constraints",
        "access": "readonly",
        "lifetime": "static"
        "children": [
          
        ]
      }
    ]
  }
