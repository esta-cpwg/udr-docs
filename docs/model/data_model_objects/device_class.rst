.. _data-model-objects-device-class:

############
Device Class
############

A **device class** defines the behavior of a **device**, which is some type of equipment that
is configurable. The term "device class" in this standard is typically used to refer to a single model of
equipment, and "device" refers to a single unit of that model.

Within the context of a particular device class, the fully qualified atomic data object identifier provides unique
identification and addressing of atomic data objects (scalar items, structured items, bulk items etc.). When migrating
between versions of a device class this allows controllers to make sense of the new mapping.

.. _data-model-objects-device-class-versioning:

Versioning
==========

Any change to a device class must result in the device version number incrementing within the *id* attribute.

It is possible for a **device class** to contain multiple versions of the same atomic data object. Controllers
can differentiate between these using the version number contained within the
:ref:`qualified class<tutorial-identifiers-qualified-identifier>` for each object. To allow receiving devices
to differentiate between the version being used by a controller, this version number must be appended to a
transmitted id seperated by a dot. For example:

Simplified example:

.. code-block:: json

  {
    "udrtype": "deviceclass",
    "id": "org.esta.dev.my-device.3",
    "description": "ESTA My Device",
    "publishdate": "2021-02-02",
    "author": "ESTA",
    "children": [
      {
        "udrtype": "scalaritems",
        "children": [
          {
            "udrtype": "scalaritem",
            "class": "org.esta.lib.gobo.1/wheel-velocity",
            "id": "velocity",
            "access": "readwrite",
            "lifetime": "runtime"
          },
          {
            "udrtype": "scalaritem",
            "class": "org.esta.lib.gobo.2/wheel-velocity",
            "id": "velocity",
            "access": "readwrite",
            "lifetime": "runtime"
          }
        ]
      }
    ]
  }

Specifying id 'velocity.2' would inform the receiver the controller intends to address the second revision of the
class: org.esta.lib.gobo.2/wheel-velocity.

**********
Attributes
**********

.. _data-model-objects-device-class-id:

ID (Required)
=============

A device class's **id** is a unique string identifying an individual device class. This string must
be a :ref:`tutorial-identifiers-qualified-identifier`.

.. _data-model-objects-device-class-description:

Description (Required)
======================

A string that describes the device in human-readable language.

.. _data-model-objects-device-class-publish-date:

Publish Date (Required)
=======================

The date this version of the device class was published, formatted as an ISO date string (``YYYY-MM-DD``).

.. _data-model-objects-device-class-author-name:

Author Name (Required)
======================

The name of the individual or organization that created this device class.

********
Children
********

A device class can have the following children:

* :ref:`data-model-objects-device-library` (single instance)
* :ref:`data-model-objects-scalar-items` (single instance)
* :ref:`data-model-objects-structured-items` (single instance)
* :ref:`data-model-objects-streaming-structured-items` (single instance)
* :ref:`data-model-objects-bulk-items` (single instance)
* :ref:`data-model-objects-localizations` (single instance)
  
.. _data-model-objects-device-class-markup:

******
Markup
******

* UDR Type: ``deviceclass``
* Members:

  =========== ========== =======================================================
  Key         Value Type Represents
  =========== ========== =======================================================
  id          string     :ref:`data-model-objects-device-class-id`
  description string     :ref:`data-model-objects-device-class-description`
  publishdate string     :ref:`data-model-objects-device-class-publish-date`
  author      string     :ref:`data-model-objects-device-class-author-name`
  =========== ========== =======================================================

Example:

.. code-block:: json

  {
    "udrtype": "deviceclass",
    "id": "com.etcconnect.dev.es1.1",
    "description": "ES1 Dimmer",
    "publishdate": "2021-02-02",
    "author": "ETC Inc.",
    "children": [
      {
        "udrtype": "devicelibrary",
        "children": [
        ]
      },
      {
        "udrtype": "scalaritems",
        "children": [
        ]
      },
      {
        "udrtype": "structureditems",
        "children": [
        ]
      },
      {
        "udrtype": "streamingstructureditems",
        "children": [
        ]
      },
      {
        "udrtype": "bulkitems",
        "children": [
        ]
      },
      {
        "udrtype": "localizations",
        "children": [
        ]
      }
    ]
  }
