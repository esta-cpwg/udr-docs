.. _data-model-objects-scalar-items:

############
Scalar Items
############

:ref:`Device Classes<data-model-objects-device-class>` can contain a maximum of one *scalaritems*
object which lists all of the controllable and/or observable scalar items of the device. When addressing
children of the scalar items object, other objects should use ``/scl/`` followed by the id of the scalar item.

********
Ordering
********

The order in which scalar items appear has no meaning; scalar items of the same class that appear in a
certain order in the scalar items list should have no impact on the order any corresponding user interface
elements might be presented.

When ordering of scalar items of the same class is required, they shall be instantiated with the same unique
identifier followed by the ``!`` delimiter and an instance number. The instance number shall be used to define
the order of like objects.

See :ref:`scalar item<atomic-data-objects-scalar-item-instance-id>` id description for further information.

********
Children
********

Scalar items can have the following children:

* :ref:`atomic-data-objects-scalar-item-instance`

.. _data-model-objects-scalar-items-markup:

Markup
======

* UDR Type: ``scalaritems``

Example:

.. code-block:: json

  {
    "udrtype": "scalaritems",
    "children": [
      {
        "udrtype": "scalaritem",
        "class": "org.esta.lib.intensity.1/intensity",
        "id": "intensity",
        "access": "readwrite",
        "lifetime": "runtime"
      }
    ]
  }
