.. _data-model-objects-localization:

############
Localization
############

:ref:`Libraries<data-model-objects-library>` and :ref:`Device Library<data-model-objects-device-library>`
objects can contain child *localization* objects which provide localized values for a specified
language/country code.

Codes must be compliant with `ISO 639-1 <https://en.wikipedia.org/wiki/ISO_639-1>`_.

**********
Attributes
**********

.. _data-model-objects-localization-code:

Code (Required)
===============

The language code, compliant with `ISO 639-1 <https://en.wikipedia.org/wiki/ISO_639-1>`_.

Children
========

A localization object can have the following children:

* :ref:`data-model-objects-localized`

.. _data-model-objects-localization-markup:

Markup
======

* UDR Type: ``localization``
* Members:

  ============== ========== ============================================================
  Key            Value Type Represents
  ============== ========== ============================================================
  code           string     :ref:`data-model-objects-localization-code`
  ============== ========== ============================================================

Example:

.. code-block:: json

  {
    "udrtype": "localization",
    "code": "en-gb",
    "children": [
      {
        "udrtype": "localized",
        "class": "org.esta.lib.intensity.1/dimmer",
        "attribute": "name",
        "value": "dimmer"
      },
      {
        "udrtype": "localized",
        "class": "org.esta.lib.intensity.1/intensity",
        "attribute": "name",
        "value": "intensity"
      }
    ]
  },
  {
    "udrtype": "localization",
    "code": "de",
    "children": [
      {
        "udrtype": "localized",
        "class": "org.esta.lib.intensity.1/dimmer",
        "attribute": "name",
        "value": "dimmer"
      },
      {
        "udrtype": "localized",
        "class": "org.esta.lib.intensity.1/intensity",
        "attribute": "name",
        "value": "intensität"
      }
    ]
  }
