.. _data-model-objects-import:

######
Import
######

An import element makes the contents of a :ref:`data-model-objects-library` available for referencing
in the current context.

**********
Attributes
**********

.. _data-model-objects-import-library:

Library (Required)
==================

The **id** of the library that is being imported.

.. _data-model-objects-import-location:

Location
========

A URI in string representation detailing where the markup of this library can be found. This allows
controllers to access library markup when they do not already know about a library.

*******
Example
*******

.. code-block:: json

  {
    "udrtype": "import",
    "library": "org.esta.lib.identification.1"
  }
