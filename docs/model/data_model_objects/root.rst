.. _data-model-objects-udr-root:

########
UDR Root
########

A UDR root object forms the root of an object model that is built from a UDR document. It contains
libraries, devices, systems, and imports, which are references to external libraries.

Children
========

A UDR root object can have the following children:

* :ref:`data-model-objects-import`
* :ref:`data-model-objects-library`
* :ref:`data-model-objects-device-class`
* :ref:`data-model-objects-system`

.. _data-model-objects-udr-root-markup:

Markup Example
==============

.. code-block:: json

  {
    "udrtype": "udr",
    "children": [
      {
        "udrtype": "import",
        "library": "org.esta.lib.intensity.1"
      },
      {
        "udrtype": "library",
        "id": "com.acme.lib.standarddefs.1",
        "description": "ACME Corp Standard Definitions",
        "publishdate": "2020-09-02",
        "author": "ACME Corp",
        "children": [
        ]
      },
      {
        "udrtype": "deviceclass",
        "class": "com.acme.dev.my-device.1",
        "description": "ACME Corp My Device",
        "publishdate": "2020-09-02",
        "author": "ACME Corp",
        "children": [
        ]
      },
      {
        "udrtype": "system",
        "id": "com.acme.sys.my-system.1",
        "description": "ACME Corp System",
        "publishdate": "2020-09-02",
        "author": "ACME Corp"
        "children": [
        ]
      }
    ]
  }
