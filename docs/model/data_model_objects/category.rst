.. _data-model-objects-category:

########
Category
########

Categories provide logical grouping of atomic data objects (scalar items, structured items, bulk
items etc.).

Category names are defined in supplementary documents paired with libraries, each of which
may be tailored for specific industry purposes. They follow a hierarchy structure, providing
progressively more specialization.

Categories are found in :ref:`Libraries<data-model-objects-library>`, or
:ref:`device library<data-model-objects-device-library>` objects.

**********
Attributes
**********

.. _data-model-objects-category-name:

Name (Required)
===============

Indicates the **name** of the defined category this object represents. This may be any
category which is defined as a child of the parent category (if part of a nested structure).

Categories must always be defined in full using the defined hierarchy. In the example below,
because `rotate` is defined as a child of `frame`, the full hierarchy must be stated.

.. code-block:: json

  {
    "udrtype": "category",
    "name": "frame",
    "children": [
      {
        "udrtype": "category",
        "name": "rotate",
        "children": [
          {

          }
        ]
      }
    ]
  }

********
Children
********

Categories can have the following children:

* :ref:`atomic-data-objects-scalar-item-class`
* :ref:`atomic-data-objects-structured-item-class`
* :ref:`atomic-data-objects-streaming-structured-item-class`
* :ref:`atomic-data-objects-bulk-item-class`

.. _data-model-objects-category-markup:

Markup
======

* UDR Type: ``category``
* Members:

  ============== ========== ============================================================
  Key            Value Type Represents
  ============== ========== ============================================================
  name           string     Any valid child name defined in a supplementary document
  ============== ========== ============================================================

Example:

.. code-block:: json

  {
    "udrtype": "category",
    "name": "frame",
    "children": [
      {
        "udrtype": "category",
        "name": "rotate",
        "children": [
          {

          }
        ]
      }
    ]
  }
