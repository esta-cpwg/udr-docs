.. _data-model-objects-library:

#######
Library
#######

A list of atomic data object classes (scalar items, structured items, bulk items etc.). Classes defined inside
a library are usable by anything that imports the library.

**********
Attributes
**********

.. _data-model-objects-library-id:

ID (Required)
================

A library's **id** is a unique string identifying an individual library. This string must be a
:ref:`tutorial-identifiers-qualified-identifier`.

.. _data-model-objects-library-description:

Description (Required)
======================

A string that describes the purpose of a library in human-readable language.

.. _data-model-objects-library-publish-date:

Publish Date (Required)
=======================

The date this version of the library was published, formatted as an ISO date string (``YYYY-MM-DD``).

.. _data-model-objects-library-author-name:

Author Name (Required)
======================

The name of the individual or organization that created this library.

********
Children
********

A library can have the following children:

* :ref:`atomic-data-objects-scalar-item-class`
* :ref:`atomic-data-objects-structured-item-class`
* :ref:`atomic-data-objects-streaming-structured-item-class`
* :ref:`atomic-data-objects-bulk-item-class`
* :ref:`data-model-objects-category`
* :ref:`data-model-objects-localization`

.. _data-model-objects-library-markup:

Markup Example
==============

* UDR Type: ``library``
* Members:

  =========== ========== =======================================================
  Key         Value Type Represents
  =========== ========== =======================================================
  id          string     :ref:`data-model-objects-library-id`
  description string     :ref:`data-model-objects-library-description`
  publishdate string     :ref:`data-model-objects-library-publish-date`
  author      string     :ref:`data-model-objects-library-author-name`
  =========== ========== =======================================================

Example:

.. code-block:: json

  {
    "udrtype": "library",
    "id": "com.acme.lib.standarddefs.1",
    "description": "ACME Corp Standard Definitions",
    "publishdate": "2020-09-02",
    "author": "ACME Corp",
    "children": [
      {
        "udrtype": "scalaritemclass",
        "id": "customscalaritem",
        "name": "Custom Scalar Item",
        "description": "My custom scalar item",
        "datatype": "string"
      },
      {
        "udrtype": "scalaritemclass",
        "id": "customscalaritem2",
        "name": "Custom Scalar Item 2",
        "description": "My custom scalar item 2",
        "datatype": "number",
        "unit": "rpm"
      }
    ]
  }
