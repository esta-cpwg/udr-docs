.. _data-model-objects-localizations:

#############
Localizations
#############

:ref:`Device Classes<data-model-objects-device-class>` can contain a maximum of one *localizations* object which
lists all of the device specific :ref:`localization<data-model-objects-localization>` objects. These objects are
made available within the context of the device and provide localized values for a specified language/country code.

********
Children
********

Localizations can have the following children:

* :ref:`data-model-objects-localization`

.. _data-model-objects-localizations-markup:

Markup
======

* UDR Type: ``localizations``

Example:

.. code-block:: json

  {
    "udrtype": "localizations",
    "children": [
      {
        "udrtype": "localization",
        "code": "en-gb",
        "children": [

        ]
      },
      {
        "udrtype": "localization",
        "code": "de",
        "children": [
          
        ]
      }
    ]
  }
