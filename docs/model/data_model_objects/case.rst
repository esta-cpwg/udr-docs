.. _data-model-objects-case:

####
Case
####

:ref:`Localized<data-model-objects-localized>` objects can contain child *case* objects which provide
human-readable values describing ranges of possible scalar item values for scalar items with a data type of
*number*, *enum* or *bool*. The value shall be a case-sensitive string. This value is intended to be
displayed in graphical interfaces, so should be kept as short as possible.

Cases should be provided for all possible values of a scalar item, and must not overlap.

When new cases are provided they replace all previously defined cases.

**********
Attributes
**********

.. _data-model-objects-case-min:

Min (Required)
==============

The minimum (inclusive) value of the range this case provides a value for.

In the case of a scalar item of data type *enum* this would be the index to which this case applies. For
scalar item of data type *bool* this attribute is limited to 0 and 1.

.. _data-model-objects-case-max:

Max
===

The maximum (inclusive) value of the range this case provides a value for.

When this attribute is not included it is assumed to be the same as *min*, i.e. it provides a value
for a single number/index.

.. _data-model-objects-case-value:

*****
Value
*****

The value of the string describing this case.

.. _data-model-objects-case-markup:

Markup
======

* UDR Type: ``case``
* Members:

  ============== ========== ============================================================
  Key            Value Type Represents
  ============== ========== ============================================================
  min            number     :ref:`data-model-objects-case-min`
  max            number     :ref:`data-model-objects-case-max`
  value          string     :ref:`data-model-objects-case-value`
  ============== ========== ============================================================

Example:

.. code-block:: json

  {
    "udrtype": "localization",
    "code": "en-gb",
    "children": [
      {
        "udrtype": "localized",
        "class": "velocity-indexed",
        "attribute":, "case",
        "children": [
          {
            "udrtype": "case",
            "min": 0,
            "value": "Slow"
          },
          {
            "udrtype": "case",
            "min": 1,
            "value": "Medium"
          },
          {
            "udrtype": "case",
            "min": 2,
            "value": "Fast"
          }
        ]
      }
    ]
  },
  {
    "udrtype": "localization",
    "code": "de",
    "children": [
      {
        "udrtype": "localized",
        "class": "velocity-indexed",
        "attribute":, "case",
        "children": [
          {
            "udrtype": "case",
            "min": 0,
            "value": "Schleppend"
          },
          {
            "udrtype": "case",
            "min": 1,
            "value": "Mittel"
          },
          {
            "udrtype": "case",
            "min": 2,
            "value": "Schnell"
          }
        ]
      }
    ]
  },
  {
    "udrtype": "localization",
    "code": "en-gb",
    "children": [
      {
        "udrtype": "localized",
        "instance": "/scl/wheel-position",
        "attribute":, "case",
        "children": [
          {
            "udrtype": "case",
            "min": 0,
            "max": 4,
            "value": "Red"
          },
          {
            "udrtype": "case",
            "min": 5,
            "max": 19,
            "value": "Red/Orange"
          },
          {
            "udrtype": "case",
            "min": 20,
            "max": 24,
            "value": "Orange"
          }
        ]
      }
    ]
  },
  {
    "udrtype": "localization",
    "code": "de",
    "children": [
      {
        "udrtype": "localized",
        "instance": "/scl/wheel-position",
        "attribute":, "case",
        "children": [
          {
            "udrtype": "case",
            "min": 0,
            "max": 4,
            "value": "Rot"
          },
          {
            "udrtype": "case",
            "min": 5,
            "max": 19,
            "value": "Rot/Orange"
          },
          {
            "udrtype": "case",
            "min": 20,
            "max": 24,
            "value": "Orange"
          }
        ]
      }
    ]
  }
