.. _data-model-objects-system:

######
System
######

WORK IN PROGRESS

A list of devices.

**********
Attributes
**********

.. _data-model-objects-system-id:

ID (Required)
=============

A system's **id** is a unique string identifying an individual system. This string must be a
:ref:`tutorial-identifiers-qualified-identifier`.

********
Children
********

A system can have the following children:

* :ref:`data-model-objects-device-instance`

.. _data-model-objects-system-markup:

Markup Example
==============

.. code-block:: json

  {
    "udrtype": "system",
    "id": "com.acme.sys.my-system.1",
    "description": "ACME Corp System",
    "publishdate": "2020-09-02",
    "author": "ACME Corp",
    "children": [
      {
        "udrtype": "device",
        "class": "com.acme.dev.my-device.1",
        "id": "mydevice"
      }
    ]
  }