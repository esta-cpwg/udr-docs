.. _data-model-objects-localized:

#########
Localized
#########

:ref:`Localizations<data-model-objects-localization>` can contain child *localized* objects.

The value of *localized* is a localized string (or strings) for the referenced atomic data
object (class or instance) and attribute specified.

**********
Attributes
**********

.. _data-model-objects-localized-class:

Class (Required*)
=================

Indicates the **id** of the defined object class for which this key provides a localized
value.

* required, and present only when the *instance* attribute is not included.

.. _data-model-objects-localized-instance:

Instance (Required*)
====================

Indicates the **id** of the object instance for which this key provides localized case values.

When *instance* is included, *attribute* must be set to *case*.

* required, and present only when the *class* attribute is not included.

.. _data-model-objects-localized-attribute:

Attribute (Required)
====================

The attribute of the object for which a localized string is being provided. **case** is a special
attribute identifier which indicates this object contains child :ref:`data-model-objects-case`
objects for a scalar item class or instance rather than one of its attributes.

The following attributes are permitted for localization:

================================= =========================
Object          Attributes
================================= =========================
Scalar Item Class                 name, description, (case)
Scalar Item                       (case)
Structured Item Class                 name, description
Structured Item                       N/A
Streaming Structured Item Class       name, description
Streaming Structured Item             N/A
Bulk Item Class                   name, description
Bulk Item                         N/A
================================= =========================

.. _data-model-objects-localized-value:

*****
Value
*****

The value of the string describing the attribute of this object for the language this key is
contained within. The value shall be a case-sensitive string. This value is intended to be
displayed in graphical interfaces, so should be kept as short as possible.

.. _data-model-objects-localized-markup:

Markup
======

* UDR Type: ``localized``
* Members:

  ============== ========== ============================================================
  Key            Value Type Represents
  ============== ========== ============================================================
  class          string     :ref:`data-model-objects-localized-class`
  instance       string     :ref:`data-model-objects-localized-instance`
  attribute      string     :ref:`data-model-objects-localized-attribute`
  value          string     :ref:`data-model-objects-localized-value`
  ============== ========== ============================================================

Example:

.. code-block:: json

  {
    "udrtype": "localization",
    "code": "en-gb",
    "children": [
      {
        "udrtype": "localized",
        "class": "come.acme.definitions.1/my-item",
        "attribute": "name",
        "value": "My Item"
      },
      {
        "udrtype": "localized",
        "class": "org.esta.lib.intensity.1/dimmer",
        "attribute": "name",
        "value": "Dimmer"
      },
      {
        "udrtype": "localized",
        "class": "org.esta.lib.intensity.1/intensity",
        "attribute": "name",
        "value": "Intensity"
      }
    ]
  },
  {
    "udrtype": "localization",
    "code": "de",
    "children": [
      {
        "udrtype": "localized",
        "class": "come.acme.definitions.1/my-item",
        "attribute": "name",
        "value": "Mein Artikel"
      },
      {
        "udrtype": "localized",
        "class": "org.esta.lib.intensity.1/dimmer",
        "attribute": "name",
        "value": "Dimmer"
      },
      {
        "udrtype": "localized",
        "class": "org.esta.lib.intensity.1/intensity",
        "attribute": "name",
        "value": "Intensität"
      }
    ]
  }
