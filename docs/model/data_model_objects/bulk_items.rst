.. _data-model-objects-bulk-items:

##########
Bulk Items
##########

:ref:`Device Classes<data-model-objects-device-class>` can contain a maximum of one *bulkitems*
object which list all of the controllable and/or observable bulk items of the device. When
addressing children of the bulk items object, other objects should use ``/bi/`` followed by the
id of the bulk item.

********
Ordering
********

The order in which bulk items appear has no meaning; bulk items of the same class that appear in a
certain order in the bulk items list should have no impact on the order any corresponding user interface
elements might be presented.

When ordering of bulk items of the same class is required, they shall be instantiated with the same unique
identifier followed by the ``!`` delimiter and an instance number. The instance number shall be used to define
the order of like objects.

See :ref:`bulk item<atomic-data-objects-bulk-item-instance-id>` id description for further information.

********
Children
********

Bulk items can have the following children:

* :ref:`atomic-data-objects-bulk-item-instance`

.. _data-model-objects-bulk-items-markup:

Markup
======

* UDR Type: ``bulkitems``

Example:

.. code-block:: json

  {
    "udrtype": "bulkitems",
    "children": [
      {
        "udrtype": "bulkitem",
        "class": "org.esta.lib.gobo.1/gobo-image",
        "id": "gobo-image!1",
        "access": "readonly",
        "lifetime": "persistent"
      }
    ]
  }
