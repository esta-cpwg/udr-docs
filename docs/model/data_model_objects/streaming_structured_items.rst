.. _data-model-objects-streaming-structured-items:

##########################
Streaming Structured Items
##########################

:ref:`Device Classes<data-model-objects-device-class>` can contain a maximum of one *streamingstructureditems*
object which list all of the controllable and/or observable streaming structured items of the device. When addressing
children of the streaming structured items object, other objects should use ``/sst/`` followed by the id of the
streaming structured item.

********
Ordering
********

The order in which streaming structured items appear has no meaning; streaming structured items of the same class that
appear in a certain order in the streaming structured items list should have no impact on the order any corresponding
user interface elements might be presented.

When ordering of streaming structured items of the same class is required, they shall be instantiated with the same
unique identifier followed by the ``!`` delimiter and an instance number. The instance number shall be used to
define the order of like objects.

See :ref:`streaming structured item<atomic-data-objects-streaming-structured-item-instance-id>` id description for further
information.

********
Children
********

Streaming structured items can have the following children:

* :ref:`atomic-data-objects-streaming-structured-item-instance`

.. _data-model-objects-streaming-structured-items-markup:

Markup
======

* UDR Type: ``streamingstructureditems``

Example:

.. code-block:: json

  {
    "udrtype": "streamingstructureditems",
    "children": [
      {
        "udrtype": "streamingstructureditem",
        "class": "org.esta.lib.core.1/stream-map",
        "id": "stream-map",
        "access": "readonly",
        "lifetime": "static"
        "children": [
          
        ]
      }
    ]
  }
