.. _data-model-objects-device-instance:

######
Device
######

WORK IN PROGRESS

A **device** is a single addressable instance of a :doc:`device_class`.

**********
Attributes
**********

Instance attributes are per-device-instance; they can vary between instances of the device.

.. _data-model-objects-device-instance-class:

Class (Required)
================

Indicates the :ref:`id<data-model-objects-device-class-id>` of the defined device class of which
this is an instance.

.. _data-model-objects-device-instance-id:

ID (Required)
=============

The **id** is a unique string identifying this device. This must be unique within the system containing the device.

If multiple devices are instantiated using the *count* attribute, each instance will be identified starting with
the unique id followed by the ``!`` delimiter and instance number (starting at 1), and incrementing by 1 for each
instance. For example "id!1" would be followed by "id!2".

An example of a complete device instance id might take the form: ``my-device!2``.

.. _data-model-objects-device-instance-count:

Count
=====

The **count** attribute provides a mechanism for instantiating multiple device instances of the same
type.

If multiple devices are instantiated using the *count* attribute, each instance will be identified starting with
the unique id followed by the ``!`` delimiter and instance number (starting at 1), and incrementing by 1 for each
instance. For example "id!1" would be followed by "id!2".

********
Children
********

There are currently no children of a device.

.. _data-model-objects-device-instance-markup:

******
Markup
******

* UDR Type: ``device``
* Members

  ============= ========== ========================================================
  Key           Value Type Represents
  ============= ========== ========================================================
  class         string     :ref:`data-model-objects-device-instance-class`
  id            string     :ref:`data-model-objects-device-instance-id`
  count         string     :ref:`data-model-objects-device-instance-count`
  ============= ========== ========================================================

Example:

.. code-block:: json

  {
    "udrtype": "device",
    "class": "com.acme.dev.my-device.1",
    "id": "mydevice"
  }
