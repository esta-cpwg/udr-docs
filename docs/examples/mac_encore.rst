#########################################
MAC Encore Performance WRM Device Example
#########################################

:download:`Download <mac_encore.json>`

.. literalinclude:: mac_encore.json
   :language: json
