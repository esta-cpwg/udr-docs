############################
Simple Dimmer Device Example
############################

:download:`Download <simple_dimmer.json>`

.. literalinclude:: simple_dimmer.json
   :language: json
  