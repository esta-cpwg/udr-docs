#########################
Multi-Cell Device Example
#########################

:download:`Download <multi_cell.json>`

.. literalinclude:: multi_cell.json
   :language: json
