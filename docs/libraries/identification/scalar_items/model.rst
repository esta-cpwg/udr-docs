.. _library-identification-scalar-item-model:

#####
Model
#####

A string identifying a device's model.

.. _library-identification-scalar-item-model-definition:

**********
Definition
**********

=============== ================================================================
ID              model
Qualified ID    org.esta.lib.identification.1/model
Name            Model
Description     Identifies a device's model name.
Data Type       string
Category        N/A
=============== ================================================================

.. _library-identification-scalar-item-model-discussion:

**********
Discussion
**********

No discussion.

*****************
Definition Markup
*****************

.. code-block:: json

  {
    "udrtype": "scalaritemclass",
    "id": "model",
    "name": "Model",
    "description": "Identifies a device's model name.",
    "datatype": "string"
  }

****************
Instance Example
****************

.. code-block:: json

  {
    "udrtype": "scalaritem",
    "class": "org.esta.lib.identification.1/manufacturer",
    "id": "model",
    "access": "readonly",
    "lifetime": "static"
  }