.. _library-identification-scalar-item-manufacturer:

############
Manufacturer
############

A string identifying a device's manufacturer.

.. _library-identification-scalar-item-manufacturer-definition:

**********
Definition
**********

=============== ================================================================
ID              manufacturer
Qualified ID    org.esta.lib.identification.1/manufacturer
Name            Manufacturer
Description     Identifies a device's manufacturer.
Data Type       string
Category        N/A
=============== ================================================================

.. _library-identification-scalar-item-manufacturer-discussion:

**********
Discussion
**********

No discussion.

*****************
Definition Markup
*****************

.. code-block:: json

  {
    "udrtype": "scalaritemclass",
    "id": "manufacturer",
    "name": "Manufacturer",
    "description": "Identifies a device's manufacturer.",
    "datatype": "string"
  }

****************
Instance Example
****************

.. code-block:: json

  {
    "udrtype": "scalaritem",
    "class": "org.esta.lib.identification.1/manufacturer",
    "id": "manufacturer",
    "access": "readonly",
    "lifetime": "static"
  }