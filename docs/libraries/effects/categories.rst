.. _library-effects-categories:

##################
Effects Categories
##################

WORK IN PROGRESS

Categories are defined in a hierarchy structure.

.. _library-effects-categories:

=============== =============== =============== ========================================
Category        Subcategory     Subcategory     Description
=============== =============== =============== ========================================                             
effects                                         Purpose here
----------------------------------------------- ----------------------------------------
.               frost                           Purpose here
--------------- ------------------------------- ----------------------------------------                              
.               prism                           Purpose here
--------------- ------------------------------- ----------------------------------------                              
.               .               select          Purpose here
.               .               rotate          Purpose here
.               animation                       Purpose here
macro                                           Purpose here
=============================================== ========================================
