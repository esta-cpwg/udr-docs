.. _library-position-categories:

###################
Position Categories
###################

WORK IN PROGRESS

Categories are defined in a hierarchy structure.

.. _library-position-categories:

=============== =============== =============== ========================================
Category        Subcategory     Subcategory     Description
=============== =============== =============== ========================================
motion                                          Purpose here
----------------------------------------------- ----------------------------------------
.               rotate                          Purpose here
.               location                        Purpose here
--------------- ------------------------------- ----------------------------------------                              
.               .               select          Purpose here
physical                                        Purpose here
=============================================== ========================================
