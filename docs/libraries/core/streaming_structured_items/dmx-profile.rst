.. _library-core-streaming-structured-item-dmx-profile:

###########
DMX Profile
###########

This streaming structured item describes a supported operating mode of a
:ref:`device<data-model-objects-device-class>` and the dmx protocol translation in that mode.

.. _library-core-streaming-structured-item-dmx-profile-definition:

**********
Definition
**********

=============== ================================================================
ID              dmx-profile
Qualified ID    org.esta.lib.core.1/dmx-profile
Name            DMX Profile
Description     Describes a supported operating mode of a device and the dmx protocol translation in that mode.
Category        N/A
=============== ================================================================

.. _library-core-streaming-structured-item-dmx-profile-discussion:

**********
Discussion
**********

This streaming structured item contains a single ``constraints`` key, the value of which references another structured item
of class ``org.esta.lib.core.1/constraints`` describing the constraints applicable to this profile.

This streaming structured item also contains a ``chunks`` key the value of which is an object with some number of chunk
objects identified by their key. Each chunk contains a ``mappinggroups`` array containg a series of mapping group objects
which describe the conditions and translated mapping of scalar item values to DMX.

Each mapping group contains a ``conditions`` key the value of which is an array of conditions, and a ``mappings`` key, the
value of which is an array of mapping objects.

Condition objects can either contain a ``match`` key describing how nested conditions are evaluated followed by a
``conditions`` key containing an array of conditions, or simple key/values describing the condition itself.

A mapping object maps potential values of :ref:`scalar items<atomic-data-objects-scalar-item-instance>` to an
equivalent value that can be encoded in DMX.

The result of the translation is applied to the chunk of which the mapping is a child.

If the values of multiple scalar items are required to calculate the appropriate chunk minimum and
maximum values, additional mapping objects may be provided as children of a mapping object using the ``mappings`` key.

Chunk Detail
============

==================== ==========================================================================================
Attribute            Description
==================== ==========================================================================================
id                   An ID for this chunk (unique within this streaming structured item) which can be referenced by within it
size (required)      Describes the size of this chunk in octets
offsets (required)   An array of offsets for each possible octet of data
mappinggroups        An array of mapping group objects which provide values for this chunk
==================== ==========================================================================================

A chunk's **id** is a string identifying an individual chunk. This must be unique within the streaming structured item containing
the chunk.

A chunk's **size** attribute describes the size of this chunk.

This can be any of the following:

======= ===============
8       8-bit data
16      16-bit data
24      24-bit data
32      32-bit data
64      64-bit data
128     128-bit data
======= ===============

A chunk's **offsets** value is an array of offsets for each possible octet of data represented by this chunk in
relation to all other chunks contained this streaming structured item.

For example, an 8-bit chunk at the start of a profile would use an offset of 0.

If the array contains a single offset, it is considered the start offset for this chunk. When multiple
offsets are provided, the number must be equal to the number of octets making up the size of the chunk.

Condition Detail
================

==================== ==========================================================================================
Attribute            Description
==================== ==========================================================================================
match                Describes how nested conditions are evaluated (any/all)
chunk                The ID of another chunk which this condition evaluates
chunkmin             The min (inclusive) value for the referenced chunk for which this condition evaluates to true
chunkmax             The max (inclusive) value for the referenced chunk for which this condition evaluates to true
conditions           An array of conditions which should be evaluated as described by match
==================== ==========================================================================================

A conditions's **match** value describes how nested conditions are evaluated. It can be either ``any`` or
``all``, indicating that either any of the children, or all children must be valid for this mapping group to be
used for mapping values. When **match** is present, only ``conditions`` should also be included.

The **chunk** attribute references another ``chunk`` object. This condition evaluates the referenced chunks value
against the **chunkmin** and **chunkmax** attributes of the condition.

Mapping Detail
==============

======================== ==========================================================================================
Attribute                Description
======================== ==========================================================================================
scalaritem (required*)   Indicates the ID of the scalar item for which this mapping translates values
min (required*)          A minimum value for the scalar item
max                      A maximum value for the scalar item
chunkmin                 The chunk value that is provided when the scalar item is at its minimum value 
chunkmax                 The chunk value that is provided when the scalar item is at its maximum value 
======================== ==========================================================================================

\*reserved/unused chunk values are indicated using a mapping which only contains chunkmin and chunkmax. In this case
only, ``scalaritem`` and ``min`` are not required.

A mapping object maps potential values of a :ref:`scalar item<atomic-data-objects-scalar-item-instance>` to an equivalent
value that can be encoded in DMX.

The result of the translation is applied to the chunk of which the mapping is a child.

If the values of multiple scalar items are required to calculate the appropriate chunk minimum and maximum values,
additional mapping objects may be provided as children of this mapping object, and the enclosing mapping
object should exclude the **chunkmin** and **chunkmax** keys.

The **min** key defines a minimum value for the scalar item which either directly or through its children
translates to the value defined in **chunkmin**. It applies in different ways when combined with different data
types:

================== =========================================================
When applied to... Minimum indicates...
================== =========================================================
number             The number's minimum value.
string             N/A
binary             N/A
boolean            The valid value (true/false)
enum               The minimum case index.
================== =========================================================

The **max** key defines a maximum value for the scalar item which either directly or through its children
translates to the value defined in **chunkmax**. It applies in different ways when combined with different data
types:

================== =========================================================
When applied to... Maximum indicates...
================== =========================================================
number             The number's maximum value.
string             N/A
binary             N/A
boolean            N/A
enum               The maximum case index.
================== =========================================================

The **chunkmin** key defines the translated value which is associated with the scalar item value defined in
**min**. This value must be within the valid range for the parent chunk object's **size** key. This
attribute is required, and present only when there is no ``mappings`` key within this mapping object.

The **chunkmax** attribute defines the translated value which is associated with the scalar item value defined in
**max**. This value must be within the valid range for the parent chunk object's **size** key. This
attribute is required, and present only when there is no ``mappings`` key within this mapping object.

Markup Structure
================

.. code-block:: json

  {
    "constraints": "/str/constraints-structured-item-id",
    "chunks": {
      "chunk-1-id": {
        "size": 8,
        "offsets": 0,
        "mappinggroups": [
          {
            "conditions": [
              {
                "match": "any",
                "conditions": [
                  {
                    "chunk": "chunk-2-id",
                    "chunkmin": 32768,
                    "chunkmax": 65535
                  }
                ]
              }
            ],
            "mappings": [
              {
                "scalaritem": "/scl/scalar-item-2-id",
                "min": -180,
                "max": 180,
                "chunkmin": 65535,
                "chunkmax": 0
              }
            ]
          },
          {
            "conditions": [
              {
                "match": "any",
                "conditions": [
                  {
                    "chunk": "chunk-2-id",
                    "chunkmin": 0,
                    "chunkmax": 32767
                  }
                ]
              }
            ],
            "mappings": [
              {
                "scalaritem": "/scl/scalar-item-3-id",
                "min": -180,
                "max": 180,
                "chunkmin": 65535,
                "chunkmax": 0
              }
            ]
          }
        ]
      },
      "chunk-2-id": {
        "size": 16,
        "offsets": [1,2],
        "mappinggroups": [
          {
            "mappings": [
              {
                "scalaritem": "/scl/scalar-item-1-id",
                "min": 1,
                "mappings": [
                  {
                    "scalaritem": "/scl/scalar-item-2-id",
                    "min": 0.1,
                    "max": 10,
                    "chunkmin": 0,
                    "chunkmax": 65535
                  }
                ]
              }
            ]
          }
        ]
      }
    }
  }

*****************
Definition Markup
*****************

.. code-block:: json

  {
    "udrtype": "streamingstructureditemclass",
    "id": "dmx-profile",
    "name": "DMX Profile",
    "description": "Describes a supported operating mode of a device and the dmx protocol translation in that mode.",
  }

****************
Instance Example
****************

.. code-block:: json

  {
    "udrtype": "streamingstructureditem",
    "class": "org.esta.lib.core.1/dmx-profile",
    "id": "standard-mode",
    "access": "readonly",
    "lifetime": "static",
    "value": {
      "constraints": "/str/standard-constraints",
      "chunks": {
        "b9": {
          "size": 8,
          "offsets": 8,
          "mappinggroups": [
            {
              "mappings": [
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 0,
                  "chunkmin": 0,
                  "chunkmax": 9
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 1,
                  "chunkmin": 10,
                  "chunkmax": 14
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 2,
                  "chunkmin": 15,
                  "chunkmax": 19
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 3,
                  "chunkmin": 20,
                  "chunkmax": 24
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 4,
                  "chunkmin": 25,
                  "chunkmax": 29
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 5,
                  "chunkmin": 30,
                  "chunkmax": 34
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 1,
                  "chunkmin": 35,
                  "chunkmax": 39
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 2,
                  "chunkmin": 40,
                  "chunkmax": 44
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 3,
                  "chunkmin": 45,
                  "chunkmax": 49
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 4,
                  "chunkmin": 50,
                  "chunkmax": 54
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 5,
                  "chunkmin": 55,
                  "chunkmax": 59
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 1,
                  "mappings": [
                    {
                      "scalaritem": "/scl/gobo-rotator-shake",
                      "min": 0.1,
                      "max": 10,
                      "chunkmin": 60,
                      "chunkmax": 89
                    }
                  ]
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 2,
                  "mappings": [
                    {
                      "scalaritem": "/scl/gobo-rotator-shake",
                      "min": 0.1,
                      "max": 10,
                      "chunkmin": 90,
                      "chunkmax": 119
                    }
                  ]
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 3,
                  "mappings": [
                    {
                      "scalaritem": "/scl/gobo-rotator-shake",
                      "min": 0.1,
                      "max": 10,
                      "chunkmin": 120,
                      "chunkmax": 149
                    }
                  ]
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 4,
                  "mappings": [
                    {
                      "scalaritem": "/scl/gobo-rotator-shake",
                      "min": 0.1,
                      "max": 10,
                      "chunkmin": 150,
                      "chunkmax": 179
                    }
                  ]
                },
                {
                  "scalaritem": "/scl/gobo-select-index",
                  "min": 5,
                  "mappings": [
                    {
                      "scalaritem": "/scl/gobo-rotator-shake",
                      "min": 0.1,
                      "max": 10,
                      "chunkmin": 180,
                      "chunkmax": 209
                    }
                  ]
                },
                {
                  "scalaritem": "/scl/gobo-select-spin",
                  "min": -300,
                  "max": 300
                  "chunkmin": 255,
                  "chunkmax": 210
                }
              ]
            }
          ]
        },
        "b10": {
          "size": 16,
          "offsets": [9],
          "mappinggroups": [
            {
              "conditions": [
                {
                  "match": "any",
                  "conditions": [
                    {
                      "chunk": "b9",
                      "chunkmin": 0,
                      "chunkmax": 34
                    },
                    {
                      "chunk": "b9",
                      "chunkmin": 60,
                      "chunkmax": 209
                    }
                  ]
                }
              ],
              "mappings": [
                {
                  "scalaritem": "/scl/gobo-rotator-rotation",
                  "min": -180,
                  "max": 180,
                  "chunkmin": 65535,
                  "chunkmax": 0
                }
              ]
            },
            {
              "conditions": [
                {
                  "match": "any",
                  "conditions": [
                    {
                      "chunk": "b9",
                      "chunkmin": 35,
                      "chunkmax": 59
                    },
                    {
                      "chunk": "b9",
                      "chunkmin": 210,
                      "chunkmax": 255
                    }
                  ]
                }
              ],
              "mappings": [
                {
                  "scalaritem": "/scl/gobo-rotator-rotation",
                  "min": 0,
                  "chunkmin": 0,
                  "chunkmax": 600
                },
                {
                  "scalaritem": "/scl/gobo-rotator-spin",
                  "min": -300,
                  "max": 300,
                  "chunkmin": 64515,
                  "chunkmax": 601
                },
                {
                  "scalaritem": "/scl/gobo-rotator-rotation",
                  "min": 90,
                  "chunkmin": 64516,
                  "chunkmax": 65535
                }
              ]
            }
          ]
        }
      }
    }
  }