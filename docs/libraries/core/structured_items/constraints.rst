.. _library-core-structured-item-constraints:

###########
Constraints
###########

This structured item provides a mechanism for determining the availability and valid range of a
:ref:`scalar item<atomic-data-objects-scalar-item-instance>` dependent on the value of one or more
other scalar items.

.. _library-core-structured-item-constraints-definition:

**********
Definition
**********

=============== ================================================================
ID              constraints
Qualified ID    org.esta.lib.core.1/constraints
Name            Constraints
Description     Contains markup defining availability and valid ranges of scalar items
Category        N/A
=============== ================================================================

.. _library-core-structured-item-constraints-discussion:

**********
Discussion
**********

This structured item contains an object with some number of key/value pairs. Each key is a unique
identifier for a *constraint group* array which is its value. Each constraint group contains
valid combinations of :ref:`scalar items<atomic-data-objects-scalar-item-instance>` and scalar item
values for a set of dependent scalar items.

A single scalar item instance in a device class shall only be included in one constraint group,
contained within an instance of a :ref:`constraints<library-core-structured-item-constraints>` data
item and shall appear in all child *combination* arrays contained within that constraint group.

A *combination* array contains a number of child objects, which refer to
:ref:`scalar items<atomic-data-objects-scalar-item-instance>` and their values, which are considered
valid alongside the other scalar items and respective values in the same combination.

Controllers and visualizers can use these combinations to establish the availability, and valid
range of a scalar item dependent on the value of one or more other scalar items.

Combination Child Detail
========================

======================== ==========================================================================================
Key                      Description
======================== ==========================================================================================
scalaritem (required)    Indicates the ID of the scalar item for which this range provides valid values
min (required)           A minimum valid value for the scalar item. A null value means this scalar item is unavailable
max                      A maximum valid value for the scalar item. Must not be included if min is a null value
======================== ==========================================================================================

The **minimum** attribute defines a minimum valid value for the scalar item. It applies in different ways when
combined with different data types:

================== =========================================================
When applied to... Minimum indicates...
================== =========================================================
number             The number's minimum value.
string             N/A
binary             N/A
boolean            The valid value (true/false)
enum               The minimum case index.
object             N/A
================== =========================================================

The **maximum** attribute defines a maximum valid value for the scalar item. It applies in different ways when
combined with different data types:

================== =========================================================
When applied to... Maximum indicates...
================== =========================================================
number             The number's maximum value.
string             N/A
binary             N/A
boolean            N/A
enum               The maximum case index.
object             N/A
================== =========================================================

Markup Structure
================

.. code-block:: json

  {
    "constraintGroup1": [
      [
        {
          "scalaritem": "/scl/scalar-item-1-id",
          "min": 0,
          "max": 1
        },
        {
          "scalaritem": "/scl/scalar-item-2-id",
          "min": null
        },
        {
          "scalaritem": "/scl/scalar-item-3-id",
          "min": null
        }
      ],
      [
        {
          "scalaritem": "/scl/scalar-item-1-id",
          "min": null
        },
        {
          "scalaritem": "/scl/scalar-item-2-id",
          "min": 0,
          "max": 1
        },
        {
          "scalaritem": "/scl/scalar-item-3-id",
          "min": 0.1,
          "max" 10
        }
      ]
    ]
  }

*****************
Definition Markup
*****************

.. code-block:: json

  {
    "udrtype": "structureditemclass",
    "id": "constraints",
    "name": "Constraints",
    "description": "A mechanism for determining the availability and valid ranges of scalar items.",
  }

****************
Instance Example
****************

.. code-block:: json

  {
    "udrtype": "structureditem",
    "class": "org.esta.lib.core.1/constraints",
    "id": "standard-constraints",
    "access": "readonly",
    "lifetime": "static",
    "value": {
      "constraintGroup1": [
        [
          {
            "scalaritem": "/scl/shutter",
            "min": 0,
            "max": 1
          },
          {
            "scalaritem": "/scl/strobe-waveform",
            "min": null
          },
          {
            "scalaritem": "/scl/strobe-frequency",
            "min": null
          }
        ],
        [
          {
            "scalaritem": "/scl/shutter",
            "min": null
          },
          {
            "scalaritem": "/scl/strobe-waveform",
            "min": 0,
            "max": 1
          },
          {
            "scalaritem": "/scl/strobe-frequency",
            "min": 0.1,
            "max": 10
          }
        ]
      ]
    }
  }