.. _library-core-structured-item-control-groups:

##############
Control Groups
##############

This structured item provides a mechanism for grouping
:ref:`scalar items<atomic-data-objects-scalar-item-instance>` into logical controllable groups.

A maximum of one instance of this structured item may be included in a
:ref:`device class<data-model-objects-device-class>`.

.. _library-core-structured-item-control-groups-definition:

**********
Definition
**********

=============== ================================================================
ID              control-groups
Qualified ID    org.esta.lib.core.1/control-groups
Name            Control Groups
Description     Contains markup defining logical controllable groups of atomic data objects
Category        N/A
=============== ================================================================

.. _library-core-structured-item-control-groups-discussion:

**********
Discussion
**********

This structured item contains an object with some number of key/value pairs. Each key is either a unique
identifier for a *control group* array which is its value, or ``items`` which indicates the value is an 
array of member items. Where the key represents a control group it must be an integer in string representation.

Items contains an array of references to atomic data objects (scalar item, bulk item, structured item etc.).

A particular atomic data object instance may appear no more than once in any control groups structured item.

Controllers can use this structured item to represent modular controllable elements of a device (often called
multi-cell).

Control Group Detail
====================

======================== ==========================================================================================
Key                      Description
======================== ==========================================================================================
id                       Indicates the ID of this control group
items                    An array of IDs of items which are considered a 'member' of this control group
======================== ==========================================================================================

The **id** must be an integer in string representation, which must be unique within the parent object. Control groups
are ordered within the parent using this field.

Markup Structure
================

.. code-block:: json

  {
    "1": [
      "1": [
        "items": [
          "/scl/scalar-item-1-id"
        ]
      ],
      "2": [
        "items": [
          "/scl/scalar-item-2-id"
        ]
      ]
    ],
    "2": [
      "1": [
        "items": [
          "/scl/scalar-item-3-id"
        ]
      ],
      "2": [
        "items": [
          "/scl/scalar-item-4-id"
        ]
      ]
    ]
  }

This example represents a structure with 2 top level groups, each of which carries 2 children. Typical selection within
a controller may look like: (Channel).1.1, (Channel).1.2, (Channel).2.1, (Channel).2.2.

*****************
Definition Markup
*****************

.. code-block:: json

  {
    "udrtype": "structureditemclass",
    "id": "control-groups",
    "name": "Control Groups",
    "description": "Contains markup defining logical controllable groups of atomic data objects.",
  }

****************
Instance Example
****************

.. code-block:: json

  {
    "udrtype": "structureditem",
    "class": "org.esta.lib.core.1/control-groups",
    "id": "control-groups",
    "access": "readonly",
    "lifetime": "static",
    "value":   {
      "1": [
        "1": [
          "items": [
            "/scl/intensity!1"
          ]
        ],
        "2": [
          "items": [
            "/scl/intensity!2"
          ]
        ]
      ],
      "2": [
        "1": [
          "items": [
            "/scl/intensity!3"
          ]
        ],
        "2": [
          "items": [
            "/scl/intensity!4"
          ]
        ]
      ]
    }
  }