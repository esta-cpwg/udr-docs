.. _library-color-scalar-item-spectrum-wavelength-start:

#########################
Spectrum Wavelength Start
#########################

The Spectral Power Distribution wavelength start.

.. _library-color-scalar-item-spectrum-wavelength-start-definition:

**********
Definition
**********

=============== ================================================================
ID              spectrum-start
Qualified ID    org.esta.lib.color.1/spectrum-start
Name            Spectrum Start
Description     The Spectral Power Distribution wavelength start.
Data Type       number
Unit            nanometers
Category        N/A
=============== ================================================================

.. _library-color-scalar-item-spectrum-wavelength-start-discussion:

**********
Discussion
**********

No discussion.

*****************
Definition Markup
*****************

.. code-block:: json

  {
    "udrtype": "scalaritemclass",
    "id": "spectrum-start",
    "name": "Spectrum Start",
    "description": "The Spectral Power Distribution wavelength start.",
    "datatype": "number",
    "unit": "nanometers"
  }

****************
Instance Example
****************

.. code-block:: json

  {
    "udrtype": "scalaritem",      
    "class": "org.esta.lib.color.1/spectrum-start",
    "id": "spectrum-start",
    "friendlyname": "Spectrum Start",
    "access": "readonly",
    "lifetime": "static"
  }