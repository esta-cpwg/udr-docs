.. _library-color-scalar-item-spectrum-wavelength-end:

#######################
Spectrum Wavelength End
#######################

The Spectral Power Distribution wavelength end.

.. _library-color-scalar-item-spectrum-wavelength-end-definition:

**********
Definition
**********

=============== ================================================================
ID              spectrum-end
Qualified ID    org.esta.lib.color.1/spectrum-end
Name            Spectrum End
Description     The Spectral Power Distribution wavelength end.
Data Type       number
Unit            nanometers
Category        N/A
=============== ================================================================

.. _library-color-scalar-item-spectrum-wavelength-end-discussion:

**********
Discussion
**********

No discussion.

*****************
Definition Markup
*****************

.. code-block:: json

  {
    "udrtype": "scalaritemclass",
    "id": "spectrum-end",
    "name": "Spectrum End",
    "description": "The Spectral Power Distribution wavelength end.",
    "datatype": "number",
    "unit": "nanometers"
  }

****************
Instance Example
****************

.. code-block:: json

  {
    "udrtype": "scalaritem",
    "class": "org.esta.lib.color.1/spectrum-end",
    "id": "spectrum-end",
    "friendlyname": "Spectrum End",
    "access": "readonly",
    "lifetime": "static"
  }