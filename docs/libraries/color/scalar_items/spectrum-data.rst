.. _library-color-scalar-item-spectrum-data:

#############
Spectrum Data
#############

The Spectral Power Distribution data of an emitter in normalized wavelength.

.. _library-color-scalar-item-spectrum-data-definition:

**********
Definition
**********

=============== ================================================================
ID              spectrum-data
Qualified ID    org.esta.lib.color.1/spectrum-data
Name            Spectrum Data
Description     The Spectral Power Distribution data of an emitter in normalized wavelength.
Data Type       array (this data type is not currently supported)
Unit            nanometers
Category        N/A
=============== ================================================================

.. _library-color-scalar-item-spectrum-data-discussion:

**********
Discussion
**********

No discussion.

*****************
Definition Markup
*****************

.. code-block:: json

  {
    "udrtype": "scalaritemclass",
    "id": "spectrum-data",
    "name": "Spectrum Data",
    "description": "The Spectral Power Distribution data of an emitter in normalized wavelength.",
    "datatype": "array",
    "unit": "nanometers"
  }

****************
Instance Example
****************

.. code-block:: json

  {
    "udrtype": "scalaritem",      
    "class": "org.esta.lib.color.1/spectrum-data",
    "id": "red-spectrum-data",
    "friendlyname": "Red Spectrum Data",
    "access": "readonly",
    "lifetime": "static"
  }