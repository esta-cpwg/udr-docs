.. _library-color-scalar-item-spectrum-wavelength-step:

########################
Spectrum Wavelength Step
########################

The Spectral Power Distribution wavelength data step.

.. _library-color-scalar-item-spectrum-wavelength-step-definition:

**********
Definition
**********

=============== ================================================================
ID              spectrum-step
Qualified ID    org.esta.lib.color.1/spectrum-step
Name            Spectrum Step
Description     The Spectral Power Distribution wavelength step.
Data Type       number
Unit            nanometers
Category        N/A
=============== ================================================================

.. _library-color-scalar-item-spectrum-wavelength-step-discussion:

**********
Discussion
**********

No discussion.

*****************
Definition Markup
*****************

.. code-block:: json

  {
    "udrtype": "scalaritemclass",
    "id": "spectrum-step",
    "name": "Spectrum Step",
    "description": "The Spectral Power Distribution wavelength step.",
    "datatype": "number",
    "unit": "nanometers"
  }

****************
Instance Example
****************

.. code-block:: json

  {
    "udrtype": "scalaritem",      
    "class": "org.esta.lib.color.1/spectrum-step",
    "id": "spectrum-step",
    "friendlyname": "Spectrum End",
    "access": "readonly",
    "lifetime": "static"
  }