.. _library-intensity-scalar-item-binary-intensity:

################
Binary Intensity
################

A binary intensity value used for non-dimming light sources.

.. _library-intensity-scalar-item-binary-intensity-definition:

**********
Definition
**********

=============== ================================================================
ID              binary-intensity
Qualified ID    org.esta.lib.intensity.1/binary-intensity
Name            Binary Intensity
Description     Represents an emitter intensity as an on/off state where 'true' indicates 'on'.
Data Type       boolean
Category        intensity
=============== ================================================================

.. _library-intensity-scalar-item-binary-intensity-discussion:

**********
Discussion
**********

No discussion.

*****************
Definition Markup
*****************

.. code-block:: json

  {
    "udrtype": "scalaritemclass",
    "id": "binary-intensity",
    "name": "Binary Intensity"
    "description": "Represents an emitter intensity as an on/off state where 'true' indicates 'on'",
    "datatype": "boolean"
  }

****************
Instance Example
****************

.. code-block:: json

  {
    "udrtype": "scalaritem",  
    "class": "org.esta.lib.intensity.1/binary-intensity",
    "id": "primary-emitter-intensity",
    "access": "readwrite",
    "lifetime": "runtime"
  }