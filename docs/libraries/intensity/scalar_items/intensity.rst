.. _library-intensity-scalar-item-intensity:

#########
Intensity
#########

A continuous intensity value represented in percent of maximum brightness.

.. _library-intensity-scalar-item-intensity-definition:

**********
Definition
**********

=============== ================================================================
ID              intensity
Qualified ID    org.esta.lib.intensity.1/intensity
Name            Intensity
Description     Represents an emitter intensity as a percentage of its maximum brightness.
Data Type       number
Unit            percent
Category        intensity
=============== ================================================================

.. _library-intensity-scalar-item-intensity-discussion:

**********
Discussion
**********

No discussion.

*****************
Definition Markup
*****************

.. code-block:: json

  {
    "udrtype": "scalaritemclass",
    "id": "intensity",
    "name": "Name",
    "description": "Represents an emitter intensity as a percentage of its maximum brightness",
    "datatype": "number",
    "unit": "percent"
  }

****************
Instance Example
****************

.. code-block:: json

  {
    "udrtype": "scalaritem",
    "class": "org.esta.lib.intensity.1/intensity",
    "id": "intensity",
    "access": "readwrite",
    "lifetime": "runtime",
    "minimum": 0,
    "maximum": 1
  }